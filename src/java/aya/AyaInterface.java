/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aya;

import iti36.gb.hatswaq.dao.entity.hibernate.Admin;
import iti36.gb.hatswaq.dao.entity.hibernate.Message;
import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import java.util.ArrayList;

/**
 *
 * @author Shika
 */
public interface AyaInterface {

    public int getMessageCount();

    public int getNotificationCount();

    public int getPindingOffersCount();

    public ArrayList<Message> getMessages();

    public ArrayList<Message> getNotifications();

    public ArrayList<Offer> getOffersForVendor(Vendor vendor, int from, int to);

    public Offer getOffer(int id);

    public Admin getAdmin(String email, String password);

    public ArrayList<Vendor> getVendor();
}
