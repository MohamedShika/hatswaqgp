/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Aya
 */
@ManagedBean
public class Bean {
    private Part file;
    private String fileContent;
    
    public void upload(){
        try {
            fileContent=new Scanner(file.getInputStream()).useDelimiter("\\A").next();
        } catch (IOException ex) {
            Logger.getLogger(Bean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public Part getFile(){
        return file;
    }
    public void setFile(Part file){
        this.file=file;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }
//    public StreamedContent getImagestream2() {
//    if( file != null ){
//        return new DefaultStreamedContent(new ByteArrayInputStream(file.getInputStream()), file.getContentType());
//    }else{
//        return new DefaultStreamedContent();
//    }
//}
}
