/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.util;

/**
 *
 * @author Aya
 */
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext; 
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
 
@ManagedBean
public class FileUploadView {
     private UploadedFile file;
      private  byte[] image;
      private String myFile;

  
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        file.getContentType();
      image=file.getContents();
        this.image = image;
    }
      
    public UploadedFile getFile() {
        image=file.getContents();
        System.out.println(image);
        return file;
    }
 
    public void setFile(UploadedFile file) {
        this.file = file;
    }
     
    public void upload() {
        if(file != null) {
            
             image= file.getContents();
             System.out.println(image);
             
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            
        }
    }
}
