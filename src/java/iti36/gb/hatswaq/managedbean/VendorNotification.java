/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import iti36.gb.hatswaq.dao.impl.hibernate.MessageDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorContacatAdminDaoImpl;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
/**
 *
 * @author Shika
 */
@ManagedBean
public class VendorNotification {

    private static int notificationsNumber = 0;
    private List<VendorCommunication> communications;
    private VendorCommunication communication = new VendorCommunication();
    VendorContacatAdminDaoImpl vendorContacatAdmin = new VendorContacatAdminDaoImpl();
    MessageDaoImpl messageImpl = new MessageDaoImpl();
    private String from;

    /**
     * @return the notificationsNumber
     */
    public int getNotificationsNumber() {
        return notificationsNumber;
    }

    /**
     * @param aNotificationsNumber the notificationsNumber to set
     */
    public void setNotificationsNumber(int aNotificationsNumber) {
        notificationsNumber = aNotificationsNumber;
    }
    
    /**
     * @return the communications
     */
    public List<VendorCommunication> getCommunications() {
        return communications;
    }

    /**
     * @param communications the communications to set
     */
    public void setCommunications(List<VendorCommunication> communications) {
        this.communications = communications;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    @PostConstruct
    public void init(){
        System.out.println("init notification...");
        updateNotifications();
    }
    
    public void updateNotifications() {
        communications = getUnseenNotification();
        System.out.println("++++++++++++++++++++++++++size = "+communications.size());  
        if (!communications.isEmpty()) {
            notificationsNumber = communications.size();
        }
        else
            notificationsNumber = 0;
    }
       
    private List<VendorCommunication> getUnseenNotification() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);       
        Vendor vendor = (Vendor) session.getAttribute("vendorSession");
        return vendorContacatAdmin.vendorNotifications(vendor.getId());
    }
    
    public String seeNotification(VendorCommunication communication){
        this.setCommunication(communication);
        communication.setSeen(true);
        communication = vendorContacatAdmin.update(communication);
        updateNotifications();
        return "VendorShowNotification";
    }

    /**
     * @return the communication
     */
    public VendorCommunication getCommunication() {
        return communication;
    }

    /**
     * @param communication the communication to set
     */
    public void setCommunication(VendorCommunication communication) {
        this.communication = communication;
    }
    
    public String vendorNotificationsAll(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);       
        Vendor vendor = (Vendor) session.getAttribute("vendorSession");
        System.out.println("notifications all and id = "+ vendor.getId());
        communications = vendorContacatAdmin.vendorNotificationsAll(vendor.getId());
        System.out.println("sizzzzzzzzzzzzzzzzzzzzzzzzzze"+communications.size());
        return "VendorNotifications";
    }
}
