/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import iti36.gb.hatswaq.dao.entity.hibernate.Branch;
import iti36.gb.hatswaq.dao.entity.hibernate.Mobile;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.impl.hibernate.AdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.BranchDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.MobileDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorDaoImpl;
import iti36.gb.hatswaq.pushnotification.PushNotification;
import iti36.gb.hatswaq.webservices.serializer.BreanchSerializer;
import iti36.gb.hatswaq.webservices.serializer.VendorSerializer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

/**
 *
 * @author Aya
 */
@ManagedBean(name = "Vendor")
@SessionScoped
public class VendorData {
    Vendor vendor;
    Mobile mobile;
    private String name;
    private String logo = "NoItemYet.jpg";
    private String representativeName;
    private String username;
    private String password;
    private boolean skip;
    Branch branch;
    boolean branchPage;
    private MapModel emptyModel;
    private String title;
    private String address;
    private String briefInfo;
    private String phone2;
    private String whatWeSell;
    @ManagedProperty(value = "#{manageVendor}")
    private ManageVendors manageVendor;
    private double lat;
    private double lng;
    private String phone;
    private boolean status;
    private boolean addBranchOnly=false;

    public boolean isAddBranchOnly() {
        return addBranchOnly;
    }

    public void setAddBranchOnly(boolean addBranchOnly) {
        this.addBranchOnly = addBranchOnly;
    }
    
    public VendorData() {
        branch=new Branch();
    }

    public ManageVendors getManageVendor() {
        return manageVendor;
    }

    public void setManageVendor(ManageVendors manageVendor) {
        this.manageVendor = manageVendor;
    }
    

    public String getWhatWeSell() {
        return whatWeSell;
    }

    public void setWhatWeSell(String whatWeSell) {
        this.whatWeSell = whatWeSell;
    }

    
    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getBriefInfo() {
        return briefInfo;
    }

    public void setBriefInfo(String briefInfo) {
        this.briefInfo = briefInfo;
    }

     private String vendorStatus;

    public String getVendorStatus() {
        return vendorStatus;
    }

    public void setVendorStatus(String vendorStatus) {
        this.vendorStatus = vendorStatus;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public boolean isBranchPage() {
        return branchPage;
    }

    public void setBranchPage(boolean branchPage) {
        this.branchPage = branchPage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getRepresentativeName() {
        return representativeName;
    }

    public void setRepresentativeName(String representativeName) {
        this.representativeName = representativeName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public String addVendor() {
        String page = null;
        VendorDaoImpl daoImpl = new VendorDaoImpl();
        AdminDaoImpl adminDaoImpl = new AdminDaoImpl();
        boolean result;
        vendor = new Vendor();
        vendor.setUsername(username);
        vendor.setLogo(logo);
        vendor.setName(name);
        vendor.setPassword(password);
        vendor.setRepresentativeName(representativeName);
        vendor.setEnabled(1);
        result = adminDaoImpl.checkVendorAvailability(vendor);
        if (result) {
            daoImpl.create(vendor);
            page = "AdminAddBranch.xhtml";

        } else {
            FacesMessage message = new FacesMessage("Please check as this vendor is registered before");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }

        return page;
    }
    
    private String fileName="NoItemYet.jpg";
    
    public String handleFileUpload(FileUploadEvent event) {
        fileName = event.getFile().getFileName();
        logo = event.getFile().getFileName();
        System.out.println("Logo=" + logo);
        try {
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return logo;
    }
    
    private void copyFile(String fileName, InputStream inputstream) {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("iti36/gb/hatswaq/local/properties/ImagePath.properties");
            Properties prop = new Properties();
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                System.out.println("File Not Found Image Path property file.");
            }
//                OutputStream out = new FileOutputStream(new File(path + fileName));
            OutputStream out = new FileOutputStream(new File(prop.getProperty("path") + fileName));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputstream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            inputstream.close();
            out.flush();
            out.close();

            System.out.println("New file created!");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        } else {
            return event.getNewStep();
        }
    }
    
   
    public String addVendorBranch() {
        String page = null;
        branchPage = false;
        boolean result;
        AdminDaoImpl adminDaoImpl = new AdminDaoImpl();
        BranchDaoImpl daoImpl = new BranchDaoImpl();
        vendor = adminDaoImpl.getVendor(username);
        branch.setVendor(vendor);
        branch.setLatitude(lat);
        branch.setLongitude(lng);
        branch.setName(title);
        branch.setAddress(address);
        branch.setAltitude(0.0);
//        branch.setBriefInfo(briefInfo);
        daoImpl.create(branch);
        branchPage = true;
        page = "AdminAddMobileVendor.xhtml";
        return page;
    }
    
    public void addVendorBranch2() {
        String page = null;
        branchPage = false;
        boolean result;
        AdminDaoImpl adminDaoImpl = new AdminDaoImpl();
        BranchDaoImpl daoImpl = new BranchDaoImpl();
        vendor = adminDaoImpl.getVendor(username);
        branch.setVendor(vendor);
        branch.setLatitude(lat);
        branch.setLongitude(lng);
        branch.setName(title);
        branch.setAddress(address);
        branch.setAltitude(0.0);
//        branch.setBriefInfo(briefInfo);
        daoImpl.create(branch);

//        branchPage = true;
//        lat = 0.0;
//        lng = 0.0;
//        address = null;
//        title=null;
//        username=null;
        

        page = "AdminAddMobileVendor.xhtml";

    }

    public String addVendorMobile() {
        String page = null;
        AdminDaoImpl adminDaoImpl = new AdminDaoImpl();
        vendor = adminDaoImpl.getVendor(username);
        mobile = new Mobile();
        branchPage = false;

        MobileDaoImpl mobileDaoImpl = new MobileDaoImpl();
        BranchDaoImpl daoImpl = new BranchDaoImpl();

        mobile.setBranch(branch);
        mobile.setVendor(vendor);

        if (phone.equals(phone2)) {
            status = false;

            mobile.setPhone(phone);
            mobile.setStatus(status);
            mobileDaoImpl.create(mobile);
            branchPage = false;
        } else {
            status = false;
            mobile.setPhone(phone);
            mobileDaoImpl.create(mobile);

            Mobile mobile2 = new Mobile();
            mobile2.setBranch(branch);
            mobile2.setVendor(vendor);
            mobile2.setPhone(phone2);

            boolean status2 = true;
            mobile2.setStatus(status2);
            mobileDaoImpl.create(mobile2);
            branchPage = true;
        }
        page = manageVendor.goToProfile(vendor);
        
        if(addBranchOnly==false){
        sendNotificationVendorAdded(vendor);
        }else{
            sendNotificationBranchAdded(vendor);
        }
        emptyModel = new DefaultMapModel();
         branchPage = true;
        lat = 0.0;
        lng = 0.0;
        address = null;
        title=null;
        username=null;
        phone=null;
        phone2=null;
        return page;
    }
      
    @PostConstruct
    public void init() {
        emptyModel = new DefaultMapModel();
    }
      
    public MapModel getEmptyModel() {
        return emptyModel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
    
    public void addMarker() {
//        Marker marker = new Marker(new LatLng(branch.getLatitude(), branch.getLongitude()), branch.getName());
        Marker marker = new Marker(new LatLng(lat, lng),title);
        emptyModel.addOverlay(marker);
        System.out.println("lat="+branch.getLatitude()+"/tlan="+branch.getLongitude()+""+title);  
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Marker Added", "Lat:" + branch.getLatitude() + ", Lng:" + branch.getLongitude()));
    }

    private void sendNotificationVendorAdded(Vendor vendor) {
        JsonArray ja = new JsonArray();
        JsonObject result = new JsonObject();
        result.addProperty("message", "New vendor is now available");
        result.addProperty("type", "AddNewVendor");
        GsonBuilder gb = new GsonBuilder().setPrettyPrinting();
        gb.registerTypeAdapter(Vendor.class, new VendorSerializer()).disableHtmlEscaping();
        Gson gson = gb.create();
        result.add("vendor", gson.toJsonTree(vendor));
        ja.add(result);
        String messageGson = gson.toJson(ja);
//        PushNotification pushNotification = new PushNotification();
//        pushNotification.sendPushNotification(messageGson);
        System.out.println(messageGson);
    }
    
    private void sendNotificationBranchAdded(Vendor vendor){
        JsonArray ja = new JsonArray();
        JsonObject result = new JsonObject();
        result.addProperty("message", "New Branch for " + vendor.getName() +" is added");
        result.addProperty("type", "AddNewBranch");
        GsonBuilder gb = new GsonBuilder().setPrettyPrinting();
        gb.registerTypeAdapter(Branch.class, new BreanchSerializer()).disableHtmlEscaping();
        Gson gson = gb.create();
        BranchDaoImpl branchDaoImpl = new BranchDaoImpl();
//        Branch myBranch = branchDaoImpl.getOneBranche(branch);
        result.add("Branch", gson.toJsonTree(branchDaoImpl.getOneBranche(branch)));
        ja.add(result);
        String messageGson = gson.toJson(ja);
//        PushNotification pushNotification = new PushNotification();
//        pushNotification.sendPushNotification(messageGson);
        System.out.println(messageGson);
        addBranchOnly=false;
    }
}
