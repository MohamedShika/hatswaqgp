/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import iti36.gb.hatswaq.pushnotification.PushNotification;
import iti36.gb.hatswaq.dao.entity.hibernate.Item;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import org.primefaces.event.FileUploadEvent;
import iti36.gb.hatswaq.dao.impl.hibernate.ItemDAOImpl;
import iti36.gb.hatswaq.webservices.serializer.ItemSerializer;
import java.util.Properties;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Aya
 */
@ManagedBean(name = "adminItem")
@SessionScoped
public class AdminItem {

    private List<Item> items;
    private Item item;
    private DataModel<Item> model;
    private ItemDAOImpl itemImpl = new ItemDAOImpl();

    public AdminItem() {
        item = new Item(null, "", false);
        items = new ArrayList<>();
        items = itemImpl.getAll(item);
        for (Item newItem : items) {
            newItem.setDeactivateText((newItem.getDeactivate()) ? "Activate" : "Deactivate");
        }
        model = new ListDataModel<>(items);
    }

    public String updateIcon(FileUploadEvent file) {
        item = model.getRowData();
        item.setIcon(file.getFile().getFileName());
        this.item = itemImpl.update(item);
        if (this.item != null) {
            item = new Item(null, "", false);
            return null;
        } else {
            return "error";
        }
    }

    public String updateItem() {
        item = model.getRowData();
        this.item = itemImpl.update(item);
        if (this.item != null) {
            item = new Item(null, "", false);
            return null;
        } else {
            return "error";
        }
    }

    public String deactivateItem() {
        item = model.getRowData();
        item.setDeactivate(!item.getDeactivate());
        if (item.getDeactivateText().equalsIgnoreCase("Deactivate")) {
            item.setDeactivateText("Activate");
        } else {
            item.setDeactivateText("Deactivate");
        }
        this.item = itemImpl.update(item);
        if (item != null) {
            item = new Item(null, "", false);
            return null;
        } else {
            return "error";
        }
    }

    /**
     * @return the items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * @return the item
     */
    public Item getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * @return the model
     */
    public DataModel<Item> getModel() {
        return model;
    }

    public void setModel(DataModel<Item> model) {
        this.model = model;
    }

    private String itemName;
    private String itemDescription;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    private String path = "D:\\GP_images\\";
    private String fileName="NoItemYet.jpg";

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;

    }

    public void handleFileUpload(FileUploadEvent event) {
        fileName = event.getFile().getFileName();
        try {
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void copyFile(String fileName, InputStream in) {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("iti36/gb/hatswaq/local/properties/ImagePath.properties");
            Properties prop = new Properties();
            if (inputStream != null) 
                prop.load(inputStream);
            else 
                System.out.println("File Not Found Image Path property file.");
            // write the inputStream to a FileOutputStream
            OutputStream out = new FileOutputStream(new File(prop.getProperty("path") + fileName));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();

            System.out.println("New file created!");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public CheckLogin getCheckLogin() {
        return checkLogin;
    }

    public void setCheckLogin(CheckLogin checkLogin) {
        this.checkLogin = checkLogin;
    }

    @ManagedProperty(value = "#{checkLogin}")
    private CheckLogin checkLogin;

    public void handleClose() {
        Item myItem = new Item();
        myItem.setName(itemName);
        
        myItem.setDescription(itemDescription);
        myItem.setIcon(fileName);
        myItem.setAdmin(checkLogin.getAdmin());
        itemImpl.create(myItem);
        fileName="NoItemYet.jpg";
        itemDescription=null;
        itemName=null;
        items = itemImpl.getAll(item);
        for (Item newItem : items) {
            newItem.setDeactivateText((newItem.getDeactivate()) ? "Activate" : "Deactivate");
        }
        model = new ListDataModel<>(items);
        JsonArray ja = new JsonArray();
        JsonObject result = new JsonObject();  
        result.addProperty("message", "New item is now available");
        result.addProperty("type", "AddNewItem");
        GsonBuilder gb=new GsonBuilder().setPrettyPrinting();
        gb.registerTypeAdapter(Item.class, new ItemSerializer()).disableHtmlEscaping();
        Gson gson= gb.create();
        result.add("item", gson.toJsonTree(myItem));
        ja.add(result);
        String messageGson= gson.toJson(ja);       
        PushNotification pushNotification = new PushNotification();
        pushNotification.sendPushNotification(messageGson);
        System.out.println(messageGson);

//        try {
//            FacesContext.getCurrentInstance().getExternalContext().dispatch("/GCMNotification?message="+messageGson+"&shareRegId=1&regId=50");
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
         
    }

}
