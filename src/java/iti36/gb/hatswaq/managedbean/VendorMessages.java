/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Message;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import iti36.gb.hatswaq.dao.impl.hibernate.AdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.MessageDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorContacatAdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorDaoImpl;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Shika
 */
@ManagedBean(name = "vendorMessages")
public class VendorMessages {

    private Message message;
    private String from, subject, body;
    MessageDaoImpl messageImpl = new MessageDaoImpl();
    VendorContacatAdminDaoImpl contacatImpl = new VendorContacatAdminDaoImpl();
    VendorDaoImpl vendorDaoImpl;
    AdminDaoImpl adminImpl = new AdminDaoImpl();
    private List<Message> messages;
    private DataModel<Message> model;
    Vendor vendor;
    @ManagedProperty("#{checkLogin}")
    private CheckLogin checkLogin;

    @PostConstruct
    public void init() {
        System.out.println("in init");
        checkLogin = new CheckLogin();
    }
      
    public String seeVendorMessage(int id) {
        message = messageImpl.getMessage(id);
        VendorCommunication vendorCommunication = (VendorCommunication) getMessage().getVendorCommunications().toArray()[0];
//        System.out.println("in seeeee msg "+vendorCommunication.isSeen()+" for msg id = "+vendorCommunication.getMessage().getId());
        vendorCommunication.setSeen(true);
        vendorCommunication = contacatImpl.update(vendorCommunication);
//        System.out.println("after "+vendorCommunication.isSeen());
        setFrom("Admin");
        checkLogin.updateVendorMessages();
        return "VendorShowMessage";
    }
    
    
    public String vendorMessagesAll(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);       
        Vendor vendor =  (Vendor) session.getAttribute("vendorSession");
        System.out.println("message all and id = "+vendor.getId());
        List<VendorCommunication> communications = contacatImpl.vendorMessages(vendor.getId());
        messages = new ArrayList<>();
        for (VendorCommunication communication : communications) {
            messages.add(communication.getMessage());
        }
        model = new ListDataModel<>(messages);
        from = "Admin";
        return "VendorMessages";
    }
    
    public String viewMessage(){
        message = model.getRowData();
        System.out.println("===== the body "+this.message.getBody());
        this.from = "Admin";
        return "VendorShowMessage";
    }

    public String sendMessageToAdmin(){
//         System.out.println("message sub = "+subject + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + getBody());
        LocalDateTime dateTimeToConvert = LocalDateTime.now();
        Date convertToDate = Date.from(dateTimeToConvert.atZone(ZoneId.systemDefault()).toInstant());
        if(getSubject().isEmpty() || getSubject() == null) {
            System.out.print("subject isEmpty");
        } else {
            Message message = new Message(getSubject(), getBody(), convertToDate, null);
            if (messageImpl.create(message)) {
                vendor = (Vendor) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("vendorSession"); 
                VendorCommunication vendorCommunication = new VendorCommunication(adminImpl.getAdmin(1), message, vendor, false, 1, false);
                if (contacatImpl.create(vendorCommunication)){
//                    System.out.println("vendorCommunication raw inserted and id = " + vendorCommunication.getId());
                    System.out.println("Message sent Successfully");                        
                }           
            }
        }
        return vendorMessagesAll();
    }
    
    /**
     * @return the message
     */
    public Message getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(Message message) {
        this.message = message;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the messages
     */
    public List<Message> getMessages() {
        return messages;
    }

    /**
     * @param messages the messages to set
     */
    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    /**
     * @return the model
     */
    public DataModel<Message> getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(DataModel<Message> model) {
        this.model = model;
    }

    /**
     * @return the checkLogin
     */
    public CheckLogin getCheckLogin() {
        return checkLogin;
    }

    /**
     * @param checkLogin the checkLogin to set
     */
    public void setCheckLogin(CheckLogin checkLogin) {
        this.checkLogin = checkLogin;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(String body) {
        this.body = body;
    }
}
