/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.impl.hibernate.AdminDaoImpl;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Aya
 */
@ManagedBean(name = "checkVendor")
@SessionScoped
public class CheckVendor implements Serializable{
    String UserNameMessage;
    String NameMessage;
    String LogoMessage;

    public String getUserNameMessage() {
        return UserNameMessage;
    }

    public void setUserNameMessage(String UserNameMessage) {
        this.UserNameMessage = UserNameMessage;
    }

    public String getNameMessage() {
        return NameMessage;
    }

    public void setNameMessage(String NameMessage) {
        this.NameMessage = NameMessage;
    }

    public String getLogoMessage() {
        return LogoMessage;
    }

    public void setLogoMessage(String LogoMessage) {
        this.LogoMessage = LogoMessage;
    }
    public String checkVendorData()
    {
        Vendor vendor=new Vendor();
        AdminDaoImpl adminDaoImpl=new AdminDaoImpl();
        String vUserName=vendor.getUsername();
        String page=null;
        boolean userName=adminDaoImpl.checkVendorUserName(vendor);
        if (userName)
            UserNameMessage="Success";
        else 
            UserNameMessage="This user name is loggin before";
        return page;
    }        
            
}
