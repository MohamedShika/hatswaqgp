/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;


import iti36.gb.hatswaq.dao.entity.hibernate.Message;

import iti36.gb.hatswaq.dao.entity.hibernate.Branch;

import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import iti36.gb.hatswaq.dao.impl.hibernate.AdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.MessageDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorContacatAdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorDaoImpl;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import java.util.Set;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Aya
 */
@ManagedBean(name = "manageVendor")
@SessionScoped
public class ManageVendors {
    
    private List<Vendor> vendors;
    private Vendor vendor, VendorProfile;
    private String name, center, subject, body;
    private boolean mapRender = false, vendorSelected = false;
    private DataModel<Vendor> model;
    private VendorDaoImpl vendorImpl = new VendorDaoImpl();
    private OfferDAOImpl offerImpl = new OfferDAOImpl();
    private String branchAddres;

    public String getBranchAddres() {
        return branchAddres;
    }

    public void setBranchAddres(String branchAddres) {
        this.branchAddres = branchAddres;
    }
    
    
    MessageDaoImpl messageImpl = new MessageDaoImpl();
    AdminDaoImpl adminImpl = new AdminDaoImpl();
    VendorContacatAdminDaoImpl vendorContacatAdminImpl = new VendorContacatAdminDaoImpl();
    private List<Vendor> selectedVendors = new ArrayList<>();
      

    
    private String AddedBy;

    public Set getMobiles() {
        return mobiles;
    }

    public void setMobiles(Set mobiles) {
        this.mobiles = mobiles;
    }
    
   
   

  private Set mobiles;

    public String getCenter() {
        return center;
    }

   

   

    public void setCenter(String center) {
        this.center = center;
    }

    public boolean isMapRender() {
        return mapRender;
    }

    public void setMapRender(boolean mapRender) {
        this.mapRender = mapRender;
    }
  
    

    @ManagedProperty(value="#{offerItem}")
    private OffersItem offersItem;

    
    public String blockVendor() {
        vendor = model.getRowData();
        updateVendorOffersStatus(vendor);
        vendor.setEnabled((vendor.getEnabled() == 1) ? 0 : 1);
        if (vendor.getVendorStatus().equalsIgnoreCase("Block")) 
            vendor.setVendorStatus("Unblock");
        else 
           vendor.setVendorStatus("Block");
        vendor = vendorImpl.update(vendor);
        if (vendor != null) {
            vendor = new Vendor();
            return null;
        } else 
            return "error";
    } 
     
    
    public String allOffers(int id){
        String page;
        ArrayList<Offer> offers = new ArrayList<>(offerImpl.getAllOffers(id)); 
        if(offers.isEmpty())
        {   page=null;
             FacesMessage message = new FacesMessage("This vendor has no offers");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        else{
        offersItem.setModel(new ListDataModel<>(offers));
        offers.stream().forEach((offer) -> {
            offer.setOfferStatus((offer.getStatus()) ? "Deactivate" : "Activate");
        });
        page="AdminVendorOffers.xhtml";
            setName(offers.get(0).getVendor().getName());
        }            
        return page;
    }

    
    private void updateVendorOffersStatus(Vendor vendor) {
        int vendorStatus = vendor.getEnabled();
        Boolean offerStatus;
        offerStatus = (vendorStatus == 1) ? Boolean.FALSE : Boolean.TRUE;
        ArrayList<Offer> offers = new ArrayList<>(vendor.getOffers());
        offers.stream().filter((offer) -> ((vendorStatus == 1) ? offer.getStatus(): !offer.getStatus())).map((offer) -> {
            offer.setStatus(offerStatus);
            return offer;
        }).forEach((offer) -> {
            offer = offerImpl.update(offer);
        });
    }
    
    
    public String goToProfile(){
        setVendorProfile(model.getRowData());
        return "AdminSeeVendorProfile";
    }
    
    
    public String goToProfile(Vendor vendorAdded){
        setVendorProfile(vendorAdded);
        return "AdminSeeVendorProfile";
    }
    
    
    public void theBranchCenter(double longt, double latt){
        setCenter(Double.toString(longt)+","+Double.toString(latt));
//        mapRender=true;
        System.out.println("");
    }
    
    
    public String sendMessageToVendor(){
//        System.out.println("message = "+getSubject() + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + getBody());
        LocalDateTime dateTimeToConvert = LocalDateTime.now();
        Date convertToDate = Date.from(dateTimeToConvert.atZone(ZoneId.systemDefault()).toInstant());
        if (!(selectedVendors.isEmpty() && subject.equals(null) && subject.isEmpty())) {
            Message message = new Message(getSubject(), getBody(), convertToDate, null);
            if (messageImpl.create(message)) {
                for (Vendor vendor : selectedVendors) {
                    VendorCommunication vendorCommunication = new VendorCommunication(adminImpl.getAdmin(1), message, vendor, false, 0, false);
                    if (vendorContacatAdminImpl.create(vendorCommunication)){
//                        System.out.println("vendorCommunication raw inserted and id = " + vendorCommunication.getId());
                        System.out.println("Message sent Successfully");                        
                    }
                }
            }
        }
        System.out.println("sorry please select vendor");
        return null;
    }
    
    
    public void vendorRowSelected(SelectEvent event){
        selectedVendors.add((Vendor) event.getObject());
//        for (Vendor selectesVendor : selectesVendors) {
//            System.out.println(selectesVendor.getName());
//        }
    }

    
    /**
     * @return the vendors
     */
    public List<Vendor> getVendors() {
        return vendors;
    }

    /**
     * @param vendors the vendors to set
     */
    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    /**
     * @return the vendor
     */
    public Vendor getVendor() {
        return vendor;
    }

    /**
     * @param vendor the vendor to set
     */
    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    /**
     * @return the model
     */
    public DataModel<Vendor> getModel() {
        vendor = new Vendor();
        vendors = new ArrayList<>();
        vendors = vendorImpl.getAll(vendor);
        for (Vendor newVendor : vendors) {
            newVendor.setVendorStatus((newVendor.getEnabled() == 1) ? "Block" : "Unblock");
        }
        model = new ListDataModel<>(getVendors());
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(DataModel<Vendor> model) {
        this.model = model;
    }

    /**
     * @return the vendorImpl
     */
    public VendorDaoImpl getVendorImpl() {
        return vendorImpl;
    }

    /**
     * @param vendorImpl the vendorImpl to set
     */
    public void setVendorImpl(VendorDaoImpl vendorImpl) {
        this.vendorImpl = vendorImpl;
    }

    /**
     * @return the offersItem
     */
    public OffersItem getOffersItem() {
        return offersItem;
    }

    /**
     * @param offersItem the offersItem to set
     */
    public void setOffersItem(OffersItem offersItem) {
        this.offersItem = offersItem;
    }   

    /**
     * @return the offerImpl
     */
    public OfferDAOImpl getOfferImpl() {
        return offerImpl;
    }

    /**
     * @param offerImpl the offerImpl to set
     */
    public void setOfferImpl(OfferDAOImpl offerImpl) {
        this.offerImpl = offerImpl;
    }

    /**
     * @return the VendorProfile
     */
    public Vendor getVendorProfile() {
        return VendorProfile;
    }

    /**
     * @param VendorProfile the VendorProfile to set
     */
    public void setVendorProfile(Vendor VendorProfile) {
        this.VendorProfile = VendorProfile;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return the selectedVendors
     */
    public List<Vendor> getSelectedVendors() {
        return selectedVendors;
    }

    /**
     * @param selectedVendors the selectedVendors to set
     */
    public void setSelectedVendors(List<Vendor> selectedVendors) {
        this.selectedVendors = selectedVendors;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the vendorSelected
     */
    public boolean isVendorSelected() {
        return vendorSelected;
    }

    /**
     * @param vendorSelected the vendorSelected to set
     */
    public void setVendorSelected(boolean vendorSelected) {
        this.vendorSelected = vendorSelected;
    }
    
    public void findBranchMobile(Branch b){
  
        System.out.println("");
}
//    @ManagedProperty(value="#{newOffer}")
//    private iti36.gb.hatswaq.managedbean.NewOffer offermanaged;
//    
//    public String goToOfferPage(){
//        offermanaged.setVendor(model.getRowData());
//        return "AdminAddOffer";
//    }
//
//    public iti36.gb.hatswaq.managedbean.NewOffer getOffermanaged() {
//        return offermanaged;
//    }
//
//    public void setOffermanaged(iti36.gb.hatswaq.managedbean.NewOffer offermanaged) {
//        this.offermanaged = offermanaged;
//    }

    
}
