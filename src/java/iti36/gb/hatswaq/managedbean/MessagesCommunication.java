/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Admin;
import iti36.gb.hatswaq.dao.entity.hibernate.Message;
import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import iti36.gb.hatswaq.dao.impl.hibernate.MessageDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorContacatAdminDaoImpl;
import iti36.gb.hatswaq.managedbean.CheckLogin.AdminMessage;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Shika
 */
@ManagedBean(name = "messages")
public class MessagesCommunication {

    private Message message;
    private String from;
    MessageDaoImpl messageImpl = new MessageDaoImpl();
    VendorContacatAdminDaoImpl contacatImpl = new VendorContacatAdminDaoImpl();
    private List<AdminMessage> messages;
    private DataModel<AdminMessage> model;
    AdminMessage adminMsg;
    private List<VendorCommunication> communications;
    
    public String seeMessage(int id) {
        message = messageImpl.getMessage(id);
        VendorCommunication vendorCommunication = (VendorCommunication) getMessage().getVendorCommunications().toArray()[0];
        vendorCommunication.setSeen(true);
        vendorCommunication = contacatImpl.update(vendorCommunication);
        switch (vendorCommunication.getDirection()) {
            case 1:
                setFrom("Vendor");
            break;
            case 2:
                setFrom("Guest");
            break;
        }
        new CheckLogin().updateMessages();
        return "AdminShowMessage";
    }

 
    public String adminMessagesAll() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);       
        Admin admin =  (Admin) session.getAttribute("adminSession");
        System.out.println("1111 message all and id = "+admin.getId());
        List<VendorCommunication> communications = contacatImpl.adminMessages(admin.getId());
        messages = new ArrayList<>();
        for (VendorCommunication communication : communications) {
            adminMsg = new CheckLogin().new AdminMessage();
            adminMsg.setSubject(communication.getMessage().getSubject());
            adminMsg.setBody(communication.getMessage().getBody());
            adminMsg.setDate(communication.getMessage().getDate());
            switch (communication.getDirection()) {
                case 1:
                    adminMsg.setFrom("Vendor");
                break;
                case 2:
                    adminMsg.setFrom("Guest");
                break;
            }
            messages.add(adminMsg);
        }
        model = new ListDataModel<>(messages);
        return "AdminMessages";
    }
    
    public String adminNotificationsAll(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);       
        Admin admin = (Admin) session.getAttribute("adminSession");
        setCommunications(contacatImpl.adminNotificationsAll(admin.getId()));
        return "AdminNotifications";
    }
    /**
     * @return the message
     */
    public Message getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(Message message) {
        this.message = message;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the messages
     */
    public List<AdminMessage> getMessages() {
        return messages;
    }

    /**
     * @param messages the messages to set
     */
    public void setMessages(List<AdminMessage> messages) {
        this.messages = messages;
    }

    /**
     * @return the model
     */
    public DataModel<AdminMessage> getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(DataModel<AdminMessage> model) {
        this.model = model;
    }

    /**
     * @return the communications
     */
    public List<VendorCommunication> getCommunications() {
        return communications;
    }

    /**
     * @param communications the communications to set
     */
    public void setCommunications(List<VendorCommunication> communications) {
        this.communications = communications;
    }
}
