/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import iti36.gb.hatswaq.dao.entity.hibernate.Admin;
import iti36.gb.hatswaq.dao.entity.hibernate.Message;
import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.OfferItem;
import iti36.gb.hatswaq.dao.entity.hibernate.Resource;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import iti36.gb.hatswaq.dao.impl.hibernate.AdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.MessageDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferItemDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.ResourceDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorContacatAdminDaoImpl;
import iti36.gb.hatswaq.pushnotification.PushNotification;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.ws.rs.core.Response;

/**
 *
 * @author Shika
 */
@ManagedBean
@SessionScoped
public class AdminNotification implements Serializable{

    private static int notificationsNumber = 0;
    private List<VendorCommunication> communications;
    private VendorCommunication communication = new VendorCommunication();
    VendorContacatAdminDaoImpl vendorContacatAdmin = new VendorContacatAdminDaoImpl();
    private String name;
    private Offer offer = new Offer();
    private List<String> images;
    private List<OfferItem> offersItems;
    private DataModel<OfferItem> dataModel;
    MessageDaoImpl messageImpl = new MessageDaoImpl();
    
    @PostConstruct
    public void init(){
        System.out.println("init notification...");
        updateNotifications();
    }
    
    public void updateNotifications() {
        communications = getUnseenNotification();
        if (!communications.isEmpty()) {
            notificationsNumber = getCommunications().size();
        }
        else
            notificationsNumber = 0;
    }
       
    private List<VendorCommunication> getUnseenNotification() {
        return vendorContacatAdmin.adminNotificationsFromVendor(1);
    }
        
    public String seeNotification(VendorCommunication communication){
        this.communication = communication;
        communication.setSeen(true);
        communication = vendorContacatAdmin.update(communication);
        updateNotifications();
        return "AdminShowNotification";
    }
    
    public String showOffer(){
        OfferDAOImpl offerDAOImpl = new OfferDAOImpl();
        System.out.println("===============in show offer============");
        System.out.println("id = "+communication.getMessage().getBody());
        this.offer = offerDAOImpl.getOffer(Integer.parseInt(communication.getMessage().getBody()));
//        System.out.println(">>>>>>>>>>>>>>>>>>>Offer of this vendor is "+ offer.getIcon()+" status " +offer.getDescription()+offer.getId()+offer.getStartDate());
        return viewOffer(offer);
    }
    
    private String viewOffer(Offer offer){
        ResourceDAOImpl resourceDAOImpl = new ResourceDAOImpl();
        List<Resource> resources = resourceDAOImpl.getResourceOffers(offer);
        System.out.println("===============Resources=============");
        for (Resource list1 : resources) {
            System.out.println("image "+list1.getImagePath());
        }
        images = new ArrayList<>();
        for (int i = 0; i < resources.size(); i++)
            images.add(i, ((Resource) resources.get(i)).getImagePath());
        System.out.println("==============Images================");
        for (String image : getImages()) {
            System.out.println(image);
        }
//        OfferItemDAOImpl offerItemDAOImpl = new OfferItemDAOImpl();
//        offersItems = offerItemDAOImpl.getAllofferItems(offer);
        offersItems = new ArrayList<>(offer.getOfferItems());
        System.out.println("==============offers items names=============");
        for (OfferItem offerItem : getOffersItems()) {
            System.out.println(offerItem.getName());
        }
        dataModel = new ListDataModel<>(offersItems);
        return "AdminAcceptanceOffer";
    }
    
    private void updateOfferStatus(){
        OfferDAOImpl offerImpl = new OfferDAOImpl();
        System.out.println("offer before "+offer.getDescription()+" status "+offer.getStatus());
        offer.setStatus(true);
        offer = offerImpl.update(offer);
        System.out.println("offer after "+offer.getDescription()+" status "+offer.getStatus());
    }
    
    private boolean sendNotificationToVendor(String subject, String body){
        LocalDateTime dateTimeToConvert = LocalDateTime.now();
        java.util.Date convertToDate = java.util.Date.from(dateTimeToConvert.atZone(ZoneId.systemDefault()).toInstant());
        Message message = new Message(subject, body, convertToDate, null);
        if (messageImpl.create(message)) {
            System.out.println("successssssssssss and id = " + message.getId());
            VendorCommunication vendorCommunication = new VendorCommunication(communication.getAdmin(), message, offer.getVendor() , true, 0, false);
            if (vendorContacatAdmin.create(vendorCommunication)){ 
                System.out.println("inserted Vendor com and id = " + vendorCommunication.getId());
                return true;
            }
        }
        return true;
    }
    
    public boolean sendNotificationToAdmin(String subject, String body,Vendor myVendor){
        LocalDateTime dateTimeToConvert = LocalDateTime.now();
        java.util.Date convertToDate = java.util.Date.from(dateTimeToConvert.atZone(ZoneId.systemDefault()).toInstant());
        Message message = new Message(subject, body, convertToDate, null);
        AdminDaoImpl adminDaoImpl =new AdminDaoImpl();
        if (messageImpl.create(message)) {
            System.out.println("successssssssssss and id = " + message.getId());
            VendorCommunication vendorCommunication = new VendorCommunication(adminDaoImpl.getAdmin(1), message, myVendor , true, 1, false);
            if (vendorContacatAdmin.create(vendorCommunication)){ 
                System.out.println("inserted Vendor com and id = " + vendorCommunication.getId());
                return true;
            }
        }
        return true;
    }
    
    public String rejectOffer(){
        //send notify to veondor
        if(sendNotificationToVendor("Admin Hatswaq", "Sorry! your offer unsuitable so it's rejected"))
            System.out.println("Notification sent success");
        return "AdminManageVendors";
    }
    
    public String acceptOffer(){
        updateOfferStatus();
        JsonArray ja = new JsonArray();
        JsonObject result = new JsonObject();
        result.addProperty("message", "New offer from "+offer.getVendor().getName()+" is now available");
        result.addProperty("type", "AddNewOffer");
        GsonBuilder gb = new GsonBuilder().setPrettyPrinting();
        Gson gson = gb.create();
        ja.add(result);
        String messageGson = gson.toJson(ja);
        PushNotification pushNotification = new PushNotification();
        pushNotification.sendPushNotification(messageGson);
        System.out.println(messageGson);
        if(sendNotificationToVendor("Admin Hatswaq", "Your offer accepted"))
            System.out.println("Notification sent success");
        return null;
    }
    
    
    /**
     * @return the notificationsNumber
     */
    public int getNotificationsNumber() {
        return notificationsNumber;
    }

    /**
     * @param aNotificationsNumber the notificationsNumber to set
     */
    public void setNotificationsNumber(int aNotificationsNumber) {
        notificationsNumber = aNotificationsNumber;
    }

    /**
     * @return the communications
     */
    public List<VendorCommunication> getCommunications() {
        return communications;
    }

    /**
     * @param communications the communications to set
     */
    public void setCommunications(List<VendorCommunication> communications) {
        this.communications = communications;
    }

    /**
     * @return the communication
     */
    public VendorCommunication getCommunication() {
        return communication;
    }

    /**
     * @param communication the communication to set
     */
    public void setCommunication(VendorCommunication communication) {
        this.communication = communication;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the images
     */
    public List<String> getImages() {
        return images;
    }

    /**
     * @param images the images to set
     */
    public void setImages(List<String> images) {
        this.images = images;
    }

    /**
     * @return the offersItems
     */
    public List<OfferItem> getOffersItems() {
        return offersItems;
    }

    /**
     * @param offersItems the offersItems to set
     */
    public void setOffersItems(List<OfferItem> offersItems) {
        this.offersItems = offersItems;
    }

    /**
     * @return the dataModel
     */
    public DataModel<OfferItem> getDataModel() {
        return dataModel;
    }

    /**
     * @param dataModel the dataModel to set
     */
    public void setDataModel(DataModel<OfferItem> dataModel) {
        this.dataModel = dataModel;
    }

    /**
     * @return the offer
     */
    public Offer getOffer() {
        return offer;
    }

    /**
     * @param offer the offer to set
     */
    public void setOffer(Offer offer) {
        this.offer = offer;
    }
}
