/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.OfferItem;
import iti36.gb.hatswaq.dao.entity.hibernate.Resource;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferItemDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.ResourceDAOImpl;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Aya
 */
@ManagedBean(name = "offerItem")
@SessionScoped
public class OffersItem implements Serializable{

    private List<Offer> offers;
    private Offer offer;
    private DataModel<Offer> model;
    private OfferDAOImpl offerImpl = new OfferDAOImpl();
    private String status;
    private List<String> images;
    private String addedBy;
    private List<OfferItem> offersItems;
    private DataModel<OfferItem> dataModel;
    private DataModel<Vendor> dataModelVendor;
    public OffersItem() {
//        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
//        Vendor vendor = (Vendor) session.getAttribute("vendor");
//        offers = new ArrayList<>(vendor.getOffers());
//        model = new ListDataModel<>(offers);
    
             
    }

    public DataModel<Vendor> getDataModelVendor() {
        return dataModelVendor;
    }

    public void setDataModelVendor(DataModel<Vendor> dataModelVendor) {
        this.dataModelVendor = dataModelVendor;
    }

    public List<OfferItem> getOffersItems() {
        return offersItems;
    }

    public void setOffersItems(List<OfferItem> offersItems) {
        this.offersItems = offersItems;
    }

    public DataModel<OfferItem> getDataModel() {
        return dataModel;
    }

    public void setDataModel(DataModel<OfferItem> dataModel) {
        this.dataModel = dataModel;
    }

    public List<String> getImages() {
        
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getStatus() {
        offer=model.getRowData();
        if(offer.getStatus())
            status="Added";
        else
            status="Waiting";
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String blockOffer() {
        String page=null;
        offer = model.getRowData();
        offer.setStatus(!offer.getStatus());
        if(offer.getVendor().getEnabled()==1){
        offer.setOfferStatus((offer.getStatus()) ? "Ignore" : "Accept");
        offer = offerImpl.update(offer);
        if (offer != null) {
            offer = new Offer();    
            
        } else {
         
           page="error"; 
        }
        }
        else{
           FacesMessage message = new FacesMessage("You can't accept this offer because vendor in blocked");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return page;
    }
    public String checkStatus(){
        String st;
        offer=model.getRowData();
             boolean addedBy=offer.getStatus();
             if(addedBy)
                 st="Admin";
             else
                 st="Vendor";
             return st;
    }
    
    /**
     * @return the offer
     */
    public Offer getOffer() {
        return offer;
    }

    /**
     * @param offer the offer to set
     */
    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    /**
     * @return the model
     */
    public DataModel<Offer> getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(DataModel<Offer> model) {
        this.model = model;
    }

    /**
     * @return the offerImpl
     */
    public OfferDAOImpl getOfferImpl() {
        return offerImpl;
    }

    /**
     * @param offerImpl the offerImpl to set
     */
    public void setOfferImpl(OfferDAOImpl offerImpl) {
        this.offerImpl = offerImpl;
    }

    /**
     * @return the offers
     */
    public List<Offer> getOffers() {
        return offers;
    }

    /**
     * @param offers the offers to set
     */
    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }
     @PostConstruct
    public void init() {
//        ResourceDAOImpl  resourceDAOImpl=new ResourceDAOImpl();
//        images=resourceDAOImpl.getResourceOffers(offer);   
////        checkStatus();
        }
    
       public String offerDetails()
       {
           String page=null;
             offer=model.getRowData();
                 ResourceDAOImpl  resourceDAOImpl=new ResourceDAOImpl();
                   List<Resource> list=resourceDAOImpl.getResourceOffers(offer);
                   images=new ArrayList<>();
                   for(int i=0;i<list.size();i++)
                       images.add(i, ((Resource)list.get(i)).getImagePath());
                 OfferItemDAOImpl offerItemDAOImpl=new OfferItemDAOImpl();
                 offersItems=offerItemDAOImpl.getAllofferItems(offer);
                 dataModel=new ListDataModel<>(offersItems);
             page="AdminVendorViewOffer"  ; 
           return page;
       }

    public String getAddedBy() {
       offer=model.getRowData();
            if(offer.isAddedBy())
                addedBy="Admin";
            else
                addedBy=offer.getVendor().getName();
       return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }    
}
