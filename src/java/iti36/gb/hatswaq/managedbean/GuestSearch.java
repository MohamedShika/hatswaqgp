/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Branch;
import iti36.gb.hatswaq.dao.entity.hibernate.Message;
import iti36.gb.hatswaq.dao.entity.hibernate.Mobile;
import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.OfferItem;
import iti36.gb.hatswaq.dao.entity.hibernate.Resource;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import iti36.gb.hatswaq.dao.impl.hibernate.AdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.BranchDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.MessageDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.MobileDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferItemDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.ResourceDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorContacatAdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorDaoImpl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Aya
 */
@ManagedBean(name = "Search")
@SessionScoped
public class GuestSearch {
    private Vendor vendor;
    private Offer offer;
    private List<Offer> offers;
    private List<Offer> offersSearch;
    private List<Vendor> vendors;
    private List vendorNames;
    private VendorDaoImpl vendorDAOImpl;
    private OfferDAOImpl offerDAOImpl;
    private Date start;
    private Date end;
    private String vendorName;
    private DataModel<Offer> dataModelOffer;
    private List<String> topRatedIcon;
    private List<Offer> topRatedOffer;
    private int rate;
    private int status;
    private int flag=0;
    private ResourceDAOImpl resourceDAOImpl;
    private List<String> images;
    private List<OfferItem> offersItems;
    private DataModel<OfferItem> dataModelItems; 
    private Message message;
    private String subject;
    private String body;
    private Set mobiles;
    private Mobile mobile;
    private DataModel<Branch> dataModelBranch;
    private Branch branch;
    private BranchDaoImpl branchDaoImpl;
    private List<Branch> branchs;

    public List<Branch> getBranchs() {
        return branchs;
    }

    public void setBranchs(List<Branch> branchs) {
        this.branchs = branchs;
    }

    
    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    
    public DataModel<Branch> getDataModelBranch() {
        branchs=new ArrayList<>();
        branchDaoImpl=new BranchDaoImpl();
        branchs=branchDaoImpl.getBranches(vendor.getId());
          dataModelBranch=new ListDataModel<>(branchs);
        return dataModelBranch;
    }

    public void setDataModelBranch(DataModel<Branch> dataModelBranch) {
        this.dataModelBranch = dataModelBranch;
    }
    
    public Mobile getMobile() {
      
        return mobile;
    }

    public void setMobile(Mobile mobile) {
        this.mobile = mobile;
    }

    
    public Set getMobiles() {
        return mobiles;
    }

    public void setMobiles(Set mobiles) {
        this.mobiles = mobiles;
    }
    
    
    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public List<Vendor> getVendors() {
        vendorDAOImpl=new VendorDaoImpl();
        vendor=new Vendor();
//        vendors=new ArrayList<Vendor>();
        vendors=vendorDAOImpl.getAllVendors();
        return vendors;
    }

    public void setVendors(List<Vendor> vendors) {
        this.vendors = vendors;
    }

    public List getVendorNames() {
         vendorDAOImpl=new VendorDaoImpl();
        vendorNames=vendorDAOImpl.getAllVendorsNames();
        return vendorNames;
    }

    public void setVendorNames(List vendorNames) {
        this.vendorNames = vendorNames;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
               

        this.vendorName = vendorName;
    }
    
    public DataModel<Offer> getDataModelOffer() {
           offerDAOImpl=new OfferDAOImpl();
          offer=new Offer();
        offers=offerDAOImpl.getAllAcceptOffers();
        for(int i=0;i<offers.size();i++)
            if(offers.get(i).getVendor().getEnabled()==0)
                 offers.remove(i);
                
         dataModelOffer=new ListDataModel<>(offers);

        return dataModelOffer;
    }

    public void setDataModelOffer(DataModel<Offer> dataModelOffer) {
        this.dataModelOffer = dataModelOffer;
    }

    public List<Offer> getTopRatedOffer() {
         offerDAOImpl=new OfferDAOImpl();
        topRatedIcon=new ArrayList<String>();
        topRatedOffer=new ArrayList<Offer>();
        topRatedOffer=offerDAOImpl.getTop10Rated();
        for(int i=0;i<topRatedOffer.size();i++)
        {  if(topRatedOffer.get(i).getVendor().getEnabled()==0)
                topRatedOffer.remove(i);
        }
        return topRatedOffer;
    }

    public void setTopRatedOffer(List<Offer> topRatedOffer) {
        this.topRatedOffer = topRatedOffer;
    }

    public List<String> getTopRatedIcon() {
        offerDAOImpl=new OfferDAOImpl();
        topRatedIcon=new ArrayList<String>();
        topRatedOffer=new ArrayList<Offer>();
        topRatedOffer=offerDAOImpl.getTop10Rated();
        for(int i=0;i<topRatedOffer.size();i++)
        {  if(topRatedOffer.get(i).getVendor().getEnabled()==0)
                topRatedOffer.remove(i);
           else
            topRatedIcon.add(topRatedOffer.get(i).getIcon());
        }
        
        return topRatedIcon;
    }

    public void setTopRatedIcon(List<String> topRatedIcon) {
        this.topRatedIcon = topRatedIcon;
    }

   

    
    public List<Offer> getOffers() {
          offerDAOImpl=new OfferDAOImpl();
          offer=new Offer();
        offers=offerDAOImpl.getAllAcceptOffers();
        for(int i=0;i<offers.size();i++)
            if(offers.get(i).getVendor().getEnabled()==0)
                offers.remove(i);
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public List<Offer> getOffersSearch() {
        return offersSearch;
    }

    public void setOffersSearch(List<Offer> offersSearch) {
        this.offersSearch = offersSearch;
    }
    
    public String searchOffer(){
        
        vendorDAOImpl=new VendorDaoImpl();
        offerDAOImpl=new OfferDAOImpl();
        vendor=vendorDAOImpl.getVendorByName(vendorName);
        offersSearch=new ArrayList<Offer>();
        if(start!=null && end !=null)
                offersSearch=offerDAOImpl.getOfferByVendorSearch(vendor,start,end);
        else
        {
            if(start !=null)
                offersSearch=offerDAOImpl.getOfferByVendorSearchStart(vendor, start);
            else if(end !=null)
                offersSearch=offerDAOImpl.getOfferByVendorSearchEnd(vendor, end);
            else
                offersSearch=offerDAOImpl.getOfferByVendorVname(vendor);
        }
        dataModelOffer =new ListDataModel<>(offersSearch);
        
        return "GuestSearchResult.xhtml";
    }  
    public void calcRate(){
        
//         offerDAOImpl=new OfferDAOImpl();
         offer=dataModelOffer.getRowData();
//         String selectedObjID = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectedObj");
//         Offer offerRate=new Offer();
//            offerRate=offerDAOImpl.calcRate(offer);
    int rateing = offer.getRate();
        offerDAOImpl.calcRate(offer);
    }
    
    public void clacRateItem(){
        OfferItem offerItem = dataModelItems.getRowData();
        System.out.println("");
    }

    public int getRate() {
        offer=dataModelOffer.getRowData();
        rate=offer.getRate();
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public String offerDetails(){
        String page=null;
        offer=dataModelOffer.getRowData();
        resourceDAOImpl=new ResourceDAOImpl();
         List<Resource> list=resourceDAOImpl.getResourceOffers(offer);
         images=new ArrayList<>();
                   for(int i=0;i<list.size();i++)
                       images.add(i, ((Resource)list.get(i)).getImagePath());
                 OfferItemDAOImpl offerItemDAOImpl=new OfferItemDAOImpl();
                 offersItems=offerItemDAOImpl.getAllofferItems(offer);
                 dataModelItems=new ListDataModel<>(offersItems);
                 page="GuestOfferAndItem.xhtml";
        return page;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<OfferItem> getOffersItems() {
        return offersItems;
    }

    public void setOffersItems(List<OfferItem> offersItems) {
        this.offersItems = offersItems;
    }

    public DataModel<OfferItem> getDataModelItems() {
        return dataModelItems;
    }

    public void setDataModelItems(DataModel<OfferItem> dataModelItems) {
        this.dataModelItems = dataModelItems;
    }
    public String sendMessge()
    {
        String page=null;
//        message=new Message();message.setId(2);
//        message.setDate(start);
            message=new Message();
            message.setSubject(subject);
            message.setBody(body);
            message.setDate(new Date());
            VendorContacatAdminDaoImpl vendorContacatAdminDaoImpl=new VendorContacatAdminDaoImpl();
        AdminDaoImpl adminDAOImpl=new AdminDaoImpl();
        MessageDaoImpl messageDaoImpl=new  MessageDaoImpl();
      if( messageDaoImpl.create(message))
      {  
          VendorCommunication vendorCommunication = new VendorCommunication(adminDAOImpl.getAdmin(1),message, null, false, 2, false);
        vendorContacatAdminDaoImpl.create(vendorCommunication);
      }subject=null;
            body=null;
            page="GuestMessageSuccess.xhtml";
            return page;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
    public String vendorProfile(){
        String page=null;
        vendor=vendorDAOImpl.getVendorByName(vendorName);
        if(vendor==null){
            FacesMessage message = new FacesMessage("Please select vendor");
            FacesContext.getCurrentInstance().addMessage(null, message);
            page=null;
        }
        else
        { branch=new Branch();
        page="GuestVendorProfile.xhtml";
        }
        return page;
    }
    public void branchMobile(){
//         branch=dataModelBranch.getRowData();
//        MobileDaoImpl mobileDaoImpl=new MobileDaoImpl();
//         mobile=mobileDaoImpl.getMobileGuest(vendor, branch);
    }
    
}
