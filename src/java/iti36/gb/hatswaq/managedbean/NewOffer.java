/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import iti36.gb.hatswaq.dao.entity.hibernate.Item;
import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.OfferItem;
import iti36.gb.hatswaq.dao.entity.hibernate.Resource;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferItemDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.ResourceDAOImpl;
import iti36.gb.hatswaq.pushnotification.PushNotification;
import iti36.gb.hatswaq.webservices.serializer.OfferItemSerializer;
import iti36.gb.hatswaq.webservices.serializer.OfferSerializer;
import iti36.gb.hatswaq.webservices.serializer.ResourceSerializer;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.http.HttpSession;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.CroppedImage;

/**
 *
 * @author Wafik Aziz
 */
@ManagedBean(name = "newOffer")
@SessionScoped
public class NewOffer {
    

    public NewOffer() {
      
            offerPicsArray = new ArrayList<>();
            offerItemList = new ArrayList<>();
            offerPicsArray.add("NoItemYetOffer.jpg");
            OfferPicsModel = new ListDataModel<>(offerPicsArray);
//       offerItemsModel = new ListDataModel<>(offerItemList);
            offerItem = new OfferItem();
            try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("iti36/gb/hatswaq/local/properties/ImagePath.properties");
            Properties prop = new Properties();
            if (inputStream != null)
             prop.load(inputStream);
                  
            else
            System.out.println("File Not Found Image Path property file.");
            
               outPath   =   prop.getProperty("path");
                resourcePath    = prop.getProperty("resource");
                System.out.println("");
        } catch (IOException ex) {
          ex.printStackTrace();
        }
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
     private String fileName="NoItemYetOffer.jpg";
     private String offerIcon="NoItemYet.jpg";
    private String offerName;
    private boolean showNamePhase=true;
    private boolean showPicPhase=false;
    private boolean showCroper=false;
    private boolean showRemoveLink=false;
    private boolean showIconCropper=false;
    private CroppedImage IconCropped;
    private DataModel<String> OfferPicsModel;
    private ArrayList<String> offerPicsArray;
    private int offerPicsNumber=0;
    private String iconPicChoosen;
    private CroppedImage croppedImage;
    private CroppedImage croppedItem;
    private Vendor vendor;
    private boolean showItemCrop=false;
    private String itemPicChosen;
    private boolean showItemCropper=false;
    private boolean showItemDialog=false;
    private OfferItem offerItem;
    private ArrayList<OfferItem> offerItemList;
    private DataModel<OfferItem> offerItemsModel;
    private boolean showTableitems=false;
    private String offerItemName;
    private String offerItemDesc;
    private String offerItemPrice;
    private Date startDate;
    private Date endDate;
    private String resourcePath;
    private String outPath;
    private boolean showSbmitbutton=false;
    private boolean comeFromAdmin=false;
    private boolean comeFromTheVendor=false;

    public boolean isComeFromTheVendor() {
        return comeFromTheVendor;
    }

    public void setComeFromTheVendor(boolean comeFromTheVendor) {
        this.comeFromTheVendor = comeFromTheVendor;
    }
    
    public boolean isComeFromAdmin() {
        return comeFromAdmin;
    }

    public void setComeFromAdmin(boolean comeFromAdmin) {
        this.comeFromAdmin = comeFromAdmin;
    }

    

    public boolean isShowSbmitbutton() {
        return showSbmitbutton;
    }

    public void setShowSbmitbutton(boolean showSbmitbutton) {
        this.showSbmitbutton = showSbmitbutton;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getOfferItemName() {
        return offerItemName;
    }

    public void setOfferItemName(String offerItemName) {
        this.offerItemName = offerItemName;
    }

    public String getOfferItemDesc() {
        return offerItemDesc;
    }

    public void setOfferItemDesc(String offerItemDesc) {
        this.offerItemDesc = offerItemDesc;
    }

    public String getOfferItemPrice() {
        return offerItemPrice;
    }

    public void setOfferItemPrice(String offerItemPrice) {
        this.offerItemPrice = offerItemPrice;
    }

    public boolean isShowTableitems() {
        return showTableitems;
    }

    public void setShowTableitems(boolean showTableitems) {
        this.showTableitems = showTableitems;
    }

    public ArrayList<OfferItem> getOfferItemList() {
        return offerItemList;
    }

    public void setOfferItemList(ArrayList<OfferItem> offerItemList) {
        this.offerItemList = offerItemList;
    }

    public DataModel<OfferItem> getOfferItemsModel() {
        return offerItemsModel;
    }

    public void setOfferItemsModel(DataModel<OfferItem> offerItemsModel) {
        this.offerItemsModel = offerItemsModel;
    }
    
    public OfferItem getOfferItem() {
        return offerItem;
    }

    public void setOfferItem(OfferItem offerItem) {
        this.offerItem = offerItem;
    }

    public boolean isShowItemDialog() {
        return showItemDialog;
    }

    public void setShowItemDialog(boolean showItemDialog) {
        this.showItemDialog = showItemDialog;
    }

    public CroppedImage getCroppedItem() {
        return croppedItem;
    }

    public void setCroppedItem(CroppedImage croppedItem) {
        this.croppedItem = croppedItem;
    }

    public boolean isShowItemCropper() {
        return showItemCropper;
    }

    public void setShowItemCropper(boolean showItemCropper) {
        this.showItemCropper = showItemCropper;
    }
    
    public String getItemPicChosen() {
        return itemPicChosen;
    }

    public void setItemPicChosen(String itemPicChosen) {
        this.itemPicChosen = itemPicChosen;
    }

    public boolean isShowItemCrop() {
        return showItemCrop;
    }

    public void setShowItemCrop(boolean showItemCrop) {
        this.showItemCrop = showItemCrop;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public String getOfferIcon() {
        return offerIcon;
    }

    public void setOfferIcon(String offerIcon) {
        this.offerIcon = offerIcon;
    }

    public CroppedImage getCroppedImage() {
        return croppedImage;
    }

    public void setCroppedImage(CroppedImage croppedImage) {
        this.croppedImage = croppedImage;
    }

    public boolean isShowIconCropper() {
        return showIconCropper;
    }

    public void setShowIconCropper(boolean showIconCropper) {
        this.showIconCropper = showIconCropper;
    }
    

    public String getIconPicChoosen() {
        return iconPicChoosen;
    }

    public void setIconPicChoosen(String IconPicChoosen) {
        this.iconPicChoosen = IconPicChoosen;
    }

    public boolean isShowRemoveLink() {
        return showRemoveLink;
    }

    public void setShowRemoveLink(boolean showRemoveLink) {
        this.showRemoveLink = showRemoveLink;
    }
    

    public int getOfferPicsNumber() {
        return offerPicsNumber;
    }

    public void setOfferPicsNumber(int offerPicsNumber) {
        this.offerPicsNumber = offerPicsNumber;
    }

    public ArrayList<String> getOfferPicsArray() {
        return offerPicsArray;
    }

    public void setOfferPicsArray(ArrayList<String> offerPicsArray) {
        this.offerPicsArray = offerPicsArray;
    }

    public DataModel<String> getOfferPicsModel() {
        return OfferPicsModel;
    }

    public void setOfferPicsModel(DataModel<String> OfferPicsModel) {
        this.OfferPicsModel = OfferPicsModel;
    }

    public boolean isShowCroper() {
        return showCroper;
    }

    public void setShowCroper(boolean showCroper) {
        this.showCroper = showCroper;
    }

    public CroppedImage getIconCropped() {
        return IconCropped;
    }

    public void setIconCropped(CroppedImage IconCropped) {
        this.IconCropped = IconCropped;
    }

    public boolean isShowPicPhase() {
        return showPicPhase;
    }

    public void setShowPicPhase(boolean showPicPhase) {
        this.showPicPhase = showPicPhase;
    }

    public boolean getShowNamePhase() {
        return showNamePhase;
    }

    public void setShowNamePhase(boolean showNamePhase) {
        this.showNamePhase = showNamePhase;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }
    
    public void checkNamePahse(){
        showNamePhase=false;
        showPicPhase=true;
    }
    
    
    
     public void handleFileUpload(FileUploadEvent event) {
//         showPicPhase=false;
//         showCroper=true;
        offerPicsNumber++;
         showRemoveLink=true;
        fileName = event.getFile().getFileName();
        offerPicsArray.remove("NoItemYetOffer.jpg");
        offerPicsArray.add(fileName);
        OfferPicsModel = new ListDataModel<>(offerPicsArray);
        try {
            copyFile(event.getFile().getFileName(), event.getFile().getInputstream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void copyFile(String fileName, InputStream in) {
        try {
           
            // write the inputStream to a FileOutputStream
            OutputStream out = new FileOutputStream(new File(outPath + fileName));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();
            String source = outPath + fileName;
            resizeUsingJavaAlgo(source,new File(outPath+fileName),650,650);
            System.out.println("New file created!");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
   public boolean resizeUsingJavaAlgo(String source, File dest, int width, int height) throws IOException {
  BufferedImage sourceImage = ImageIO.read(new FileInputStream(source));
  double ratio = (double) sourceImage.getWidth()/sourceImage.getHeight();
  if (width < 1) {
    width = (int) (height * ratio + 0.4);
  } else if (height < 1) {
    height = (int) (width /ratio + 0.4);
  }

  Image scaled = sourceImage.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
  BufferedImage bufferedScaled = new BufferedImage(scaled.getWidth(null), scaled.getHeight(null), BufferedImage.TYPE_INT_RGB);
  Graphics2D g2d = bufferedScaled.createGraphics();
  g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
  g2d.drawImage(scaled, 0, 0, width, height, null);
  dest.createNewFile();
  writeJpeg(bufferedScaled, dest.getCanonicalPath(), 1.0f);
  writeJpeg(bufferedScaled, resourcePath+fileName, 1.0f);
  
  
//            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("iti36/gb/hatswaq/local/properties/ImagePath.properties");
//            Properties prop = new Properties();
//            if (inputStream != null) 
//                prop.load(inputStream);
//            
//            else 
//                System.out.println("File Not Found Image Path property file.");
//   copyFileUsingFileStreams(new File(prop.getProperty("path") + fileName),new File ("D:\\git\\hatswaqgp\\web\\resources\\images\\IconCroped\\"+fileName));
  return true;
}

   private static void writeJpeg(BufferedImage image, String destFile, float quality)
      throws IOException {
  ImageWriter writer = null;
  FileImageOutputStream output = null;
  try {
    writer = ImageIO.getImageWritersByFormatName("jpeg").next();
    ImageWriteParam param = writer.getDefaultWriteParam();
    param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
    param.setCompressionQuality(quality);
    output = new FileImageOutputStream(new File(destFile));
    writer.setOutput(output);
    IIOImage iioImage = new IIOImage(image, null, null);
    writer.write(null, iioImage, param);
    
  } catch (IOException ex) {
    throw ex;
  } finally {
    if (writer != null) {
      writer.dispose();
    }
    if (output != null) {
      output.close();
    }
  }
}
   public void removeOfferPic(){
        offerPicsArray.remove(OfferPicsModel.getRowData());
        offerPicsNumber--;
        if(offerPicsNumber<0){
            offerPicsNumber=0;
        }
        if(offerPicsArray.isEmpty()){
            offerPicsArray.add("NoItemYetOffer.jpg");
            showRemoveLink=false;
        }
       OfferPicsModel = new ListDataModel<>(offerPicsArray);
   }
   public void showPicPhasePrevious(){
       showPicPhase=false;
       showNamePhase=true;
   }
    public void showPicPhaseNext(){
       showPicPhase=false;
       showCroper=true;
   }
    public void showCroperPrevious(){
       showPicPhase=true;
       showCroper=false;
   }
    public void showCroperNext(){
       showCroper=false;
       showTableitems =true;
   }
    
    public void showTablePrevios(){
        showTableitems =false;
        showCroper=true;
       
    }
    
    public void chooseIconPic(){
       
            iconPicChoosen="/resources/images/IconCroped/"+OfferPicsModel.getRowData();
            showIconCropper=true;
            showCroper=false;
       
    }
    public void chooseItempic(){
        itemPicChosen="/resources/images/IconCroped/"+OfferPicsModel.getRowData();
        showItemCrop=false;
        showItemCropper=true;
    }
    public String changeIconPic(){
        showCroper=true;
        showIconCropper=false;
        return "AdminAddOffer";
    }
    public void changeItemPic(){
        showItemCropper=false;
        showItemCrop=true;
    }
    
    private static void copyFileUsingFileStreams(File source, File dest)
        throws IOException {
    InputStream input = null;
    OutputStream output = null;
    try {
        input = new FileInputStream(source);
        output = new FileOutputStream(dest);
        byte[] buf = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buf)) > 0) {
            output.write(buf, 0, bytesRead);
        }
    } finally {
        input.close();
        output.close();
    }
}

     public void crop() {
        if(croppedImage == null) {
            return;
        }        
        FileImageOutputStream imageOutput;
        try {
            offerIcon = offerName+"_Icon.jpg";
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("iti36/gb/hatswaq/local/properties/ImagePath.properties");
            Properties prop = new Properties();
            if (inputStream != null) 
                prop.load(inputStream);
            else 
                System.out.println("File Not Found Image Path property file.");
            
            imageOutput = new FileImageOutputStream(new File(prop.getProperty("path") + offerIcon));
            imageOutput.write(croppedImage.getBytes(), 0, croppedImage.getBytes().length);
            imageOutput.close();
            showCroper=true;
        showIconCropper=false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     public void cropItem() {
        if(croppedImage == null) {
            return;
        }        
        FileImageOutputStream imageOutput;
        try {
           
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("iti36/gb/hatswaq/local/properties/ImagePath.properties");
            Properties prop = new Properties();
            if (inputStream != null) 
                prop.load(inputStream);
            else 
                System.out.println("File Not Found Image Path property file.");
            
            
            offerItem.setName(offerItemName);
            imageOutput = new FileImageOutputStream(new File(prop.getProperty("path") + offerItem.getName()+".JPG"));
            imageOutput.write(croppedItem.getBytes(), 0, croppedItem.getBytes().length);
            imageOutput.close();
            offerItem.setImage(offerItem.getName()+".JPG");
            offerItem.setDescription(offerItemDesc);
            offerItem.setPrice(Float.parseFloat(offerItemPrice));
            offerItemList.add(offerItem);
            offerItemsModel = new ListDataModel<>(offerItemList);
            offerItem=new OfferItem();
            offerItemName=null;
            offerItemDesc=null;
            offerItemPrice=null;
            showItemCropper=false;
            showSbmitbutton=true;
            showTableitems=true;
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
     public void showItemPrevious(){
         showItemCrop=false;
         showCroper=true;
     }
    
     public void cropFromTable(){
         if(!offerItemName.equals("") && !offerItemPrice.equals("") ){
             showTableitems=false;
         showItemCrop=true;
         }
     }
     
     @ManagedProperty(value = "#{manageVendor}")
    private ManageVendors manageVendors;

    public ManageVendors getManageVendors() {
        return manageVendors;
    }

    public void setManageVendors(ManageVendors manageVendors) {
        this.manageVendors = manageVendors;
    }
     
    public String submitOffer(){
        Offer offer = new Offer();
        offer.setDescription(offerName);
        offer.setIcon(offerIcon);
        offer.setStatus(true);
        offer.setVendor(vendor);
        offer.setAddedBy(true);
        offer.setStartDate(startDate);
        offer.setEndDate(endDate);
        offer.setUser(0);
        offer.setRate(0);
        offer.setTotalRating(0);
        OfferDAOImpl offerDAOImpl = new OfferDAOImpl();
        offerDAOImpl.create(offer);
        ResourceDAOImpl resourceDAOImpl = new ResourceDAOImpl();
        OfferItemDAOImpl offerItemDAOImpl = new OfferItemDAOImpl();
       
        for(String offerPic:offerPicsArray){
            Resource resource = new Resource();
            resource.setOffer(offer);
            resource.setImagePath(offerPic);
           resourceDAOImpl.create(resource);
        }
        
        for(OfferItem offerItemList:offerItemList){
            offerItemList.setOffer(offer);
            offerItemList.setTotalRating(0);
            offerItemDAOImpl.create(offerItemList);
        }
        
//        File temp = new File(resourcePath);
//        for(File file: temp.listFiles()){
//            file.delete();
//        }
           
            String reslut;
            reslut = manageVendors.allOffers(vendor.getId());
            
            Offer pushOffer = offerDAOImpl.getOffer(offer.getId());
            
//            JsonArray ja = new JsonArray();
//            JsonObject result = new JsonObject();
//            result.addProperty("message", "New offer from "+vendor.getName()+" is now available");
//            result.addProperty("type", "AddNewOffer");
//            GsonBuilder gb=new GsonBuilder().setPrettyPrinting();
//            gb.registerTypeAdapter(Offer.class, new OfferSerializer()).disableHtmlEscaping();
//            gb.registerTypeAdapter(Resource.class, new ResourceSerializer()).disableHtmlEscaping();
//            gb.registerTypeAdapter(OfferItem.class, new OfferItemSerializer()).disableHtmlEscaping();
//            Gson gson= gb.create();
//            result.add("offer", gson.toJsonTree(offer));
//            result.add("offer images", gson.toJsonTree(pushOffer.getResources()));
//            result.add("offer items", gson.toJsonTree(pushOffer.getOfferItems()));
//            ja.add(result);
//            String messageGson= gson.toJson(ja);
//            System.out.println(messageGson);
//            
//            System.out.println("");

//             OfferDAOImpl offerDAOImpl = new OfferDAOImpl();
        if(offerDAOImpl.create(offer))
            sendNotificationOffer(offer);
            
             vendor=null;
             
        return reslut;
    
    }
    
    public String submitOfferfromVendor(){
        Offer offer = new Offer();
        offer.setDescription(offerName);
        offer.setIcon(offerIcon);
        offer.setStatus(false);
        offer.setVendor(vendor);
        offer.setAddedBy(false);
        offer.setStartDate(startDate);
        offer.setEndDate(endDate);
        offer.setUser(0);
        offer.setRate(0);
        offer.setTotalRating(0);
       
        ResourceDAOImpl resourceDAOImpl = new ResourceDAOImpl();
        OfferItemDAOImpl offerItemDAOImpl = new OfferItemDAOImpl();
       OfferDAOImpl offerDAOImpl = new OfferDAOImpl();
        offerDAOImpl.create(offer);
        for(String offerPic:offerPicsArray){
            Resource resource = new Resource();
            resource.setOffer(offer);
            resource.setImagePath(offerPic);
           resourceDAOImpl.create(resource);
        }
        
        for(OfferItem offerItemList:offerItemList){
            offerItemList.setOffer(offer);
            offerItemList.setTotalRating(0);
            offerItemDAOImpl.create(offerItemList);
        }
        AdminNotification adminNotification = new AdminNotification();
        adminNotification.sendNotificationToAdmin("offer added from " + vendor.getName(),offer.getId().toString(), vendor);
        startNewOffer();
        return "VendorOffers";
    }
    
    public void onDateSelect(SelectEvent event) {
        startDate=(Date)event.getObject();
    }
    
    public void removeItem(){
        offerItem = offerItemsModel.getRowData();
        offerItemList.remove(offerItem);
        offerItemsModel = new ListDataModel<>(offerItemList);
        if(offerItemList.isEmpty()){
            showSbmitbutton=false;
        }
   
      
    }
    public void modifyItem(){
       OfferItem offerItem2 = offerItemsModel.getRowData();
       int index = offerItemsModel.getRowIndex();
       offerItemList.get(index).setName(offerItem2.getName());
       offerItemList.get(index).setPrice(offerItem2.getPrice());
       offerItemList.get(index).setDescription(offerItem2.getDescription());
        offerItemsModel = new ListDataModel<>(offerItemList);
  
    }
    
    public void startNewOffer(){
    showNamePhase=true;
    showPicPhase=false;
     showCroper=false;
     showRemoveLink=false;
     showIconCropper=false;
     showItemCrop=false;
     showTableitems=false;
     showSbmitbutton=false;
     offerName=null;
     IconCropped = null;
     offerPicsNumber=0;
     iconPicChoosen=null;
     croppedImage=null;
     croppedItem=null;
     itemPicChosen=null;
     offerItem=null;
     offerItemName=null;
     offerItemDesc=null;
     offerItemPrice=null;
     startDate=null;
     endDate=null;
     offerIcon="NoItemYet.jpg";
     comeFromAdmin=false;
     comeFromTheVendor=false;
     
     offerPicsArray = new ArrayList<>();
     offerItemList = new ArrayList<>();
     offerPicsArray.add("NoItemYetOffer.jpg");
     OfferPicsModel = new ListDataModel<>(offerPicsArray);
     offerItemsModel = new ListDataModel<>(offerItemList);
     offerItem = new OfferItem();
          
    }
    
    public String chooseNewVendor(){
         startNewOffer();
         comeFromAdmin=true;
          return "AdminAddOffer";
    }
    
    public void checkVendor(){
        if(vendor==null){
            try {
                ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
                context.redirect("AdminManageVendors");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
       
    }
    
    public String comeFromeVendor(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);       
        vendor = (Vendor) session.getAttribute("vendorSession");    
        comeFromTheVendor=true;
        return "VendorAddOffer?faces-redirect=true";
    }

    private void sendNotificationOffer(Offer offer) {
        JsonArray ja = new JsonArray();
        JsonObject result = new JsonObject();
        result.addProperty("message", "New offer from "+offer.getVendor().getName()+" is now available");
        result.addProperty("type", "AddNewOffer");
        GsonBuilder gb = new GsonBuilder().setPrettyPrinting();
        Gson gson = gb.create();
        ja.add(result);
        String messageGson = gson.toJson(ja);
        PushNotification pushNotification = new PushNotification();
        pushNotification.sendPushNotification(messageGson);
        System.out.println(messageGson);
    }
}
