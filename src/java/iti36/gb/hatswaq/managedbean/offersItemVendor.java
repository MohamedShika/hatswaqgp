/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Message;
import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.OfferItem;
import iti36.gb.hatswaq.dao.entity.hibernate.Resource;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import iti36.gb.hatswaq.dao.impl.hibernate.AdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.MessageDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferItemDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.ResourceDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorContacatAdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorDaoImpl;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Aya
 */
@ManagedBean(name = "offerItemVendor")
@SessionScoped
public class offersItemVendor {
     
    List offers;
    private DataModel<Offer> model;
    private String status;
    private List<String> images;
    private String addedBy;
    List<OfferItem> offersItems;
    Offer offer;
    Vendor vendor;
    ListDataModel<OfferItem> dataModel;
    
    public List getOffers() {
        return offers;
    }

    public void setOffers(List offers) {
        this.offers = offers;
    }


    public DataModel<Offer> getModel() {
    vendor = (Vendor) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("vendorSession");
        offers=new ArrayList();
        OfferDAOImpl offerDAOImpl=new OfferDAOImpl();
        offers= offerDAOImpl.getAllOffers(vendor.getId());
          model = new ListDataModel<>(offers);
        return model;
    }

    public void setModel(DataModel<Offer> model) {
        this.model = model;
    }

   

    public String getStatus() {
        offer=model.getRowData();
        if(offer.getStatus())
            status="Added";
        else
            status="Waiting";
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getAddedBy() {
         offer=model.getRowData();
            if(offer.isAddedBy())
                addedBy="Admin";
            else
                addedBy=offer.getVendor().getName();
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public List<OfferItem> getOffersItems() {
        return offersItems;
    }

    public void setOffersItems(List<OfferItem> offersItems) {
        this.offersItems = offersItems;
    }

    public ListDataModel<OfferItem> getDataModel() {
        return dataModel;
    }

    public void setDataModel(ListDataModel<OfferItem> dataModel) {
        this.dataModel = dataModel;
    }
    
    public String offerDetails()
       {
           String page=null;
             offer=model.getRowData();
                 ResourceDAOImpl  resourceDAOImpl=new ResourceDAOImpl();
                   List<Resource> list=resourceDAOImpl.getResourceOffers(offer);
                   images=new ArrayList<>();
                   for(int i=0;i<list.size();i++)
                       images.add(i, ((Resource)list.get(i)).getImagePath());
                 OfferItemDAOImpl offerItemDAOImpl=new OfferItemDAOImpl();
         offersItems = offerItemDAOImpl.getAllofferItems(offer);
         dataModel = new ListDataModel<>(offersItems);
             page="VendorViewOffer.xhtml"  ; 
           return page;
       }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

}
