/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Admin;
import iti36.gb.hatswaq.dao.entity.hibernate.Message;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import iti36.gb.hatswaq.dao.impl.hibernate.AdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.MessageDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorDaoImpl;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Aya
 */
@ManagedBean(name = "checkLogin")
@SessionScoped
public class CheckLogin implements Serializable {

    public static int messageNumber = 0;
    private List<AdminMessage> adminMessages;
    private String userName;
    private String password;
    private Admin admin;
    private Vendor vendor;
    AdminDaoImpl adminDaoImpl;
    VendorDaoImpl vendorDaoImpl;

    public class AdminMessage {

        private int id;
        private String subject;
        private String body;
        private String from;
        private Date date;

        /**
         * @return the id
         */
        public int getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(int id) {
            this.id = id;
        }

        /**
         * @return the subject
         */
        public String getSubject() {
            return subject;
        }

        /**
         * @param subject the subject to set
         */
        public void setSubject(String subject) {
            this.subject = subject;
        }

        /**
         * @return the body
         */
        public String getBody() {
            return body;
        }

        /**
         * @param body the body to set
         */
        public void setBody(String body) {
            this.body = body;
        }

        /**
         * @return the from
         */
        public String getFrom() {
            return from;
        }

        /**
         * @param from the from to set
         */
        public void setFrom(String from) {
            this.from = from;
        }

        /**
         * @return the date
         */
        public Date getDate() {
            return date;
        }

        /**
         * @param date the date to set
         */
        public void setDate(Date date) {
            this.date = date;
        }
    }

    public String checkUser() {
        System.out.println("inside check");
        String page = "AdminVendorLogin";
        setAdmin(new Admin());
        setVendor(new Vendor());
        adminDaoImpl = new AdminDaoImpl();
        vendorDaoImpl = new VendorDaoImpl();
        setAdmin(adminDaoImpl.checkAdmin(getUserName(), getPassword()));
        if (getAdmin() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("adminSession", admin);
            updateMessages();
            System.out.println("messageNumber = " + messageNumber);
//            return "AdminMainPage";
            return "AdminManageVendors";
        }
        setVendor(vendorDaoImpl.checkVendor(getUserName(), getPassword()));
        if (vendor != null){
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("vendorSession", vendor);
            updateVendorMessages();
            System.out.println("messageNumber = " + messageNumber);
            page = "VendorSeeHisProfile";
        } else {
            FacesMessage message = new FacesMessage("user name or password is incorrect");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return page;
    }

    public void permission() {
        if (getAdmin() == null) {
            try {
                System.out.println("*** The user has no permission to visit this page. *** ");
                ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
                context.redirect("AdminVendorLogin.xhtml");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            System.out.println("*** The session is still active. User is logged in. *** ");
        }
    }

    public void permissionVendor() {
        if (getVendor() == null) {
            try {
                System.out.println("*** The user has no permission to visit this page. *** ");
                ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
                context.redirect("AdminVendorLogin.xhtml");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            System.out.println("*** The session is still active. User is logged in. *** ");
        }
    }

    public String logout() {
        System.out.println("in LogOut fun");
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/AdminVendorLogin?faces-redirect=true";
    }

    private List<Object[]> getUnseenMessage() {
        MessageDaoImpl messageImpl = new MessageDaoImpl();
        return messageImpl.unseenAdminMessages(1);
    }

    private void fillMessages(List<Object[]> messages) {
        adminMessages = new ArrayList<>();
        AdminMessage adminMessage = null;
        for (Object[] message : messages) {
            adminMessage = new AdminMessage();
            adminMessage.id = (int) message[0];
            adminMessage.subject = (String) message[1];
            adminMessage.body = (String) message[2];
            adminMessage.date = (Date) message[3];
            adminMessage.from = ((int) message[4] == 0) ? "Admin" : (((int) message[4] == 1) ? "Vendor" : "guest");
            adminMessages.add(adminMessage);
        }
    }

    public void updateMessages() {
        List<Object[]> messages = getUnseenMessage();
        if (!messages.isEmpty()) {
            fillMessages(messages);
            messageNumber = messages.size();
        }
        else
            messageNumber = 0;
    }
    
    public void updateVendorMessages(){
        List<Object[]> messages = getUnseenVendorMessage();
        if (!messages.isEmpty()) {
            fillMessages(messages);
            messageNumber = messages.size();
        }
        else
            messageNumber = 0;
    }
    
    private List<Object[]> getUnseenVendorMessage() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        vendor = (Vendor) session.getAttribute("vendorSession");
        MessageDaoImpl messageImpl = new MessageDaoImpl();
        return messageImpl.unseenVendorMessages(vendor.getId());
    }

    /**
     * @return the adminMessages
     */
    public List<AdminMessage> getAdminMessages() {
//        for (AdminMessage adminMessage : adminMessages) {
//        System.out.println("===================Vendor Msgs=========================");
//            System.out.println(adminMessage.from);
//            System.out.println(adminMessage.body);
//            System.out.println(adminMessage.subject);
//            System.out.println(adminMessage.date);
//        }
        return adminMessages;
    }

    /**
     * @param adminMessages the adminMessages to set
     */
    public void setAdminMessages(List<AdminMessage> adminMessages) {
        this.adminMessages = adminMessages;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the messageNumber
     */
    public int getMessageNumber() {
        return messageNumber;
    }

    /**
     * @return the admin
     */
    public Admin getAdmin() {
        return admin;
    }

    /**
     * @param admin the admin to set
     */
    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    /**
     * @return the vendor
     */
    public Vendor getVendor() {
        return vendor;
    }

    /**
     * @param vendor the vendor to set
     */
    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }
}
