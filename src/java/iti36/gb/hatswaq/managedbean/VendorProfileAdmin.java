/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Wafik Aziz
 */
@ManagedBean (name="VendorAdminProfile")
@SessionScoped
public class VendorProfileAdmin {
    
    @ManagedProperty (value="#{manageVendor}")
    ManageVendors manageVendors;

    public VendorProfileAdmin() {
    }
    
    String logo=manageVendors.getModel().getRowData().getLogo();
//    String name = manageVendors.getModel().getRowData().getName();
//
    public String getLogo() {
        return logo;
    }

    public ManageVendors getManageVendors() {
        return manageVendors;
    }

    public void setManageVendors(ManageVendors manageVendors) {
        this.manageVendors = manageVendors;
    }
//
    public void setLogo(String logo) {
        this.logo = logo;
    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
    
    public void thetry(){
        System.out.println("");
    }
}
