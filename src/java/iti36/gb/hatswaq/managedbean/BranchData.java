/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Branch;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.impl.hibernate.AdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.BranchDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorDaoImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.hibernate.Session;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

/**
 *
 * @author Aya
 */
@ManagedBean(name = "Branch",eager = true)
@SessionScoped
public class BranchData {
    private String name;
     private String briefInfo;
     private String address;
     private double altitude=0.0;
     boolean branchPage;
     private MapModel emptyModel;
      
    private String title;
      
    private double lat;
      
    private double lng;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBriefInfo() {
        return briefInfo;
    }

    public void setBriefInfo(String briefInfo) {
        this.briefInfo = briefInfo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    
    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public boolean isBranchPage() {
        return branchPage;
    }

    public void setBranchPage(boolean branchPage) {
        this.branchPage = branchPage;
    }
     
   
    public String addVendorBranch()
    {
        branchPage=false;
        Vendor v=new Vendor();
         BranchDaoImpl daoImpl=new BranchDaoImpl();
         
         Branch branch=new Branch();
         branch.setAddress(address);
         v.setId(8);
         branch.setVendor(v);
         branch.setAltitude(altitude);
         branch.setLatitude(lat);
         branch.setLongitude(lng);
         branch.setName(title);
         branch.setBriefInfo(briefInfo);
         daoImpl.create(branch);
         branchPage=true;
        return null;
    }
    public String addBranchPage(){
        return "AdminAddBranch.xhtml";
    }
    
  
    @PostConstruct
    public void init() {
        emptyModel = new DefaultMapModel();
    }
      
    public MapModel getEmptyModel() {
        return emptyModel;
    }
      
    public String getTitle() {
        return title;
    }
  
    public void setTitle(String title) {
        this.title = title;
    }
  
    public double getLat() {
        return lat;
    }
  
    public void setLat(double lat) {
        this.lat = lat;
    }
  
    public double getLng() {
        return lng;
    }
  
    public void setLng(double lng) {
        this.lng = lng;
    }
      
    public void addMarker() {
        Marker marker = new Marker(new LatLng(lat, lng), title);
        emptyModel.addOverlay(marker);
        System.out.println("lat="+lat+"/tlan="+lng);  
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Marker Added", "Lat:" + lat + ", Lng:" + lng));
        BranchData branchData=new BranchData();
//        branchData.setMapData(lat, lat, title);
    }
}
