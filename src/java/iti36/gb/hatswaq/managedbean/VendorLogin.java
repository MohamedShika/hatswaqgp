/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.managedbean;

import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import java.io.Serializable;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Shika
 */
@ManagedBean(name = "vendorLogin")
@SessionScoped
public class VendorLogin implements Serializable{

    @ManagedProperty("#{checkLogin}")
    private CheckLogin checkLogin;
    private Vendor vendor;
    private Set mobiles;

    public VendorLogin() {
//        System.out.println("in constructor");
    }
    
    @PostConstruct
    public void init() {
        vendor = checkLogin.getVendor();
//        System.out.println(vendor.getName() +"  "+vendor.getLogo());
    }
    

    /**
     * @return the mobiles
     */
    public Set getMobiles() {
        return mobiles;
    }

    /**
     * @param mobiles the mobiles to set
     */
    public void setMobiles(Set mobiles) {
        this.mobiles = mobiles;
    }

    /**
     * @return the checkLogin
     */
    public CheckLogin getCheckLogin() {
        return checkLogin;
    }

    /**
     * @param checkLogin the checkLogin to set
     */
    public void setCheckLogin(CheckLogin checkLogin) {
        this.checkLogin = checkLogin;
    }

    /**
     * @return the vendor
     */
    public Vendor getVendor() {
        return vendor;
    }

    /**
     * @param vendor the vendor to set
     */
    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }
}
