/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.webservices;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;


/**
 *
 * @author Shika
 */
    public interface Services {

    public String getItems();

    public String getOffers();

    public String getOfferImage(String id);
    
    public String getOfferItems(String id);

    public String getVendors();

    public String topRatedOffer();

    public String vendorLocation(String id);
    
    public Response getGuestMessage(String json);
    
    public void rateOffer(String offerId, String rate);
            
    public void rateItem(String itemId, String rate);
}
