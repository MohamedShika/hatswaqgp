/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.webservices.serializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import iti36.gb.hatswaq.dao.entity.hibernate.Branch;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.webservices.impl.WebServices;
import java.lang.reflect.Type;
/**
 *
 * @author Shika
 */
public class VendorSerializer implements JsonSerializer<Vendor> {

    final String ip = WebServices.getLocalIpAddress();
    
    @Override
    public JsonElement serialize(Vendor vendor, Type type, JsonSerializationContext context) {
        GsonBuilder gb = new GsonBuilder().setPrettyPrinting();;
        Gson gson;
        JsonObject root = new JsonObject();
        root.addProperty("id", vendor.getId());
        root.addProperty("name", vendor.getName());
        root.addProperty("logo",  ip + vendor.getLogo());
        root.addProperty("enabled", vendor.getEnabled());
        gb.registerTypeAdapter(Branch.class, new BreanchSerializer()).disableHtmlEscaping();
        gson = gb.create();
        root.add("branches", gson.toJsonTree(vendor.getBranches()));
        return root;
    }
}
