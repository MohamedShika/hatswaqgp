/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.webservices.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import iti36.gb.hatswaq.dao.entity.hibernate.Branch;
import iti36.gb.hatswaq.dao.entity.hibernate.Mobile;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Shika
 */
public class BreanchSerializer implements JsonSerializer<Branch> {

    @Override
    public JsonElement serialize(Branch branch, Type type, JsonSerializationContext context) {
        JsonObject root = new JsonObject();
        root.addProperty("branchName", branch.getName());
        root.addProperty("longitude", branch.getLongitude());
        root.addProperty("latitude", branch.getLatitude());
        root.addProperty("altitude", branch.getAltitude());
        root.addProperty("adress", branch.getAddress());
        List<Mobile> mobiles = new ArrayList<>(branch.getMobiles());
        root.addProperty("mobile", mobiles.get(0).getPhone());
        return root;
    }
}
