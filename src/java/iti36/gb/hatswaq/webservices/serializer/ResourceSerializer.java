/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.webservices.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import iti36.gb.hatswaq.dao.entity.hibernate.Resource;
import iti36.gb.hatswaq.webservices.impl.WebServices;
import java.lang.reflect.Type;

/**
 *
 * @author Wafik Aziz
 */
public class ResourceSerializer implements JsonSerializer<Resource> {
    
    final String ip = WebServices.getLocalIpAddress();

    @Override
    public JsonElement serialize(Resource t, Type type, JsonSerializationContext jsc) {
         JsonObject root = new JsonObject();
         root.addProperty("image", ip+ t.getImagePath());
         return root;
    }

}