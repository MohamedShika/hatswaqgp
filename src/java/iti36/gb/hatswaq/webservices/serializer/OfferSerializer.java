/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.webservices.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.webservices.impl.WebServices;
import java.lang.reflect.Type;
import java.text.Format;
import java.text.SimpleDateFormat;

/**
 *
 * @author Shika
 */
public class OfferSerializer implements JsonSerializer<Offer>{
    final String ip = WebServices.getLocalIpAddress();

    @Override
    public JsonElement serialize(Offer offer, Type type, JsonSerializationContext context) {
        JsonObject root = new JsonObject();
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        root.addProperty("id", offer.getId());
        root.addProperty("description", offer.getDescription());
        root.addProperty("startDate", formatter.format(offer.getStartDate()));
        root.addProperty("endDate", formatter.format(offer.getEndDate()));
        root.addProperty("status", offer.getStatus());
        root.addProperty("rate", offer.getRate());
        root.addProperty("icon", ip + offer.getIcon());
        return root;
    }

}
