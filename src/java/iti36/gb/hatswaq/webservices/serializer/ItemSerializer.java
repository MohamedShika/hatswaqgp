/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.webservices.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import iti36.gb.hatswaq.dao.entity.hibernate.Item;
import iti36.gb.hatswaq.webservices.impl.WebServices;
import java.lang.reflect.Type;

/**
 *
 * @author Shika
 */
public class ItemSerializer implements JsonSerializer<Item> {

    final String ip = WebServices.getLocalIpAddress();
    
    @Override
    public JsonElement serialize(Item item, Type type, JsonSerializationContext context) {
        JsonObject root = new JsonObject();
        root.addProperty("id", item.getId());
        root.addProperty("name", item.getName());
        root.addProperty("description", item.getDescription());
        root.addProperty("icon",  ip + item.getIcon());
        root.addProperty("deactivate", item.getDeactivate());
        return root;
    }
}
