/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.webservices.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import iti36.gb.hatswaq.dao.entity.hibernate.Branch;
import iti36.gb.hatswaq.dao.entity.hibernate.Item;
import iti36.gb.hatswaq.dao.entity.hibernate.Message;
import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.OfferItem;
import iti36.gb.hatswaq.dao.entity.hibernate.Resource;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import iti36.gb.hatswaq.dao.impl.hibernate.AdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.BranchDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.ItemDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.MessageDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.OfferItemDAOImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorContacatAdminDaoImpl;
import iti36.gb.hatswaq.dao.impl.hibernate.VendorDaoImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import iti36.gb.hatswaq.webservices.Services;
import iti36.gb.hatswaq.webservices.serializer.BreanchSerializer;
import iti36.gb.hatswaq.webservices.serializer.ItemSerializer;
import iti36.gb.hatswaq.webservices.serializer.OfferItemSerializer;
import iti36.gb.hatswaq.webservices.serializer.OfferSerializer;
import iti36.gb.hatswaq.webservices.serializer.ResourceSerializer;
import iti36.gb.hatswaq.webservices.serializer.VendorSerializer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Shika
 */
@Path("/webservice")
public class WebServices implements Services {

    ItemDAOImpl itemImpl = new ItemDAOImpl();
    OfferDAOImpl offerImpl = new OfferDAOImpl();
    VendorDaoImpl vendorImpl = new VendorDaoImpl();
    BranchDaoImpl branchImpl = new BranchDaoImpl();
    MessageDaoImpl messageImpl = new MessageDaoImpl();
    AdminDaoImpl adminImpl = new AdminDaoImpl();
    VendorContacatAdminDaoImpl vendorContacatAdminImpl = new VendorContacatAdminDaoImpl();
    GsonBuilder gb;
    Gson gson;

    public WebServices() {
        gb = new GsonBuilder().setPrettyPrinting();
    }

    @GET
    @Produces("application/json")
    @Path("/items")
    @Override
    public String getItems() {
        Item item = new Item();
        List<Item> items = new ArrayList<>(itemImpl.getAll(item));
        gb.registerTypeAdapter(Item.class, new ItemSerializer()).disableHtmlEscaping();
        gson = gb.create();
        return gson.toJson(items);
    }

    @GET
    @Path("/offers")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String getOffers() {
        Offer offer = new Offer();
        List<Offer> offers = new ArrayList<>(offerImpl.getAllAcceptOffers());
        gb.registerTypeAdapter(Offer.class, new OfferSerializer()).disableHtmlEscaping();
        gson = gb.create();
        return gson.toJson(offers);
    }
    @GET
    @Path("/offerImage")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.TEXT_PLAIN)
    @Override
    public String getOfferImage(String id) {
        Offer offer = offerImpl.getOffer(Integer.parseInt(id));
        return getLocalIpAddress() + offer.getIcon();
    }

    @GET
    @Path("/offerItems/{varX}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.TEXT_PLAIN)
    @Override
    public String getOfferItems(@PathParam("varX") String id) {
        Offer offer = offerImpl.getOffer(Integer.parseInt(id));
        gb.registerTypeAdapter(OfferItem.class, new OfferItemSerializer()).disableHtmlEscaping();
        gson = gb.create();
        return gson.toJson(offer.getOfferItems());
    }

    @GET
    @Path("/vendors")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String getVendors() {
        Vendor vendor = new Vendor();
        List<Vendor> vendors = new ArrayList<>(vendorImpl.getAllVendors());
        
        gb.registerTypeAdapter(Vendor.class, new VendorSerializer()).disableHtmlEscaping();
        gson = gb.create();
        return gson.toJson(vendors);
    }

    @POST
    @Path("/vendorLocation")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.TEXT_PLAIN)
    @Override
    public String vendorLocation(String id) {
        List<Branch> branchs = branchImpl.getBranches(Integer.parseInt(id));
        gb.registerTypeAdapter(Branch.class, new BreanchSerializer()).disableHtmlEscaping();
        gson = gb.create();
        return gson.toJson(branchs);
    }

    public static String getLocalIpAddress() {
        InetAddress ip = null;
        String myIp = null;
        try {
            ip = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            System.out.println("Unknown Ip address...");
        }
        myIp = (ip.getHostAddress() != null) ? ip.getHostAddress() : "localhost";
        return "http://" + myIp + ":8084/HatswaqGP/faces/images/dynamic/?file=";
    }


    @POST
    @Path("/getMsg")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_JSON)
    @Override
    public Response getGuestMessage(String json) {
       if (json != null) {
            try {
                JSONObject obj = new JSONObject(json);
                LocalDateTime dateTimeToConvert = LocalDateTime.now();
                Date convertToDate = Date.from(dateTimeToConvert.atZone(ZoneId.systemDefault()).toInstant());
                Message message = new Message(obj.get("subject").toString(), obj.get("body").toString(), convertToDate, null);
                if (messageImpl.create(message)) {
//                    System.out.println("successssssssssss and id = " + message.getId());
                    VendorCommunication vendorCommunication = new VendorCommunication(adminImpl.getAdmin(1), message, null, false, 2, false);
                    if (vendorContacatAdminImpl.create(vendorCommunication)) {
                        System.out.println("inserted Vendor com and id = " + vendorCommunication.getId());
                        return Response.status(200).entity("Ok").build();
                    }
                }
                else
                    return Response.status(500).entity("Insert failure message").build();
            } catch (JSONException ex) {
                Logger.getLogger(WebServices.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return Response.status(500).entity("Can't parse json Error").build();
    }

    @GET
    @Path("/top10rated")
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String topRatedOffer() {
        List<Offer> offers = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class).
                add(Restrictions.ge("rate", 3)).addOrder(Order.desc("rate")).setMaxResults(10).list();
        gb.registerTypeAdapter(Offer.class, new OfferSerializer()).disableHtmlEscaping();
        gson = gb.create();
        return gson.toJson(offers);
    }
    
    @GET
    @Path("/offerImages/{varX}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.TEXT_PLAIN)
    public String getOfferImages(@PathParam("varX") String id) {
//         JsonObject result = new JsonObject();        
        Offer offer = offerImpl.getOffer(Integer.parseInt(id));        
        gb.registerTypeAdapter(Resource.class, new ResourceSerializer()).disableHtmlEscaping();
        gson = gb.create();
//        result.add("images", gson.toJsonTree(offer.getResources()));
//        return gson.toJson(result);
        return gson.toJson(offer.getResources());
    }
    
    @GET
    @Path("/VendorOffers/{varX}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.TEXT_PLAIN)
    public String getOfferForVendor(@PathParam("varX") String id) {
        Vendor vendor = new Vendor();
        vendor = (Vendor) vendorImpl.getVendorByID(Integer.parseInt(id));
        gb.registerTypeAdapter(Offer.class, new OfferSerializer()).disableHtmlEscaping();
        gson = gb.create();
//        result.add("images", gson.toJsonTree(offer.getResources()));
//        return gson.toJson(result);
        return gson.toJson(vendor.getOffers());
    }
    
    static final String REG_ID_STORE = "E:\\GCMRegId.txt";
    
    @GET
    @Path("/RegId/{varX}")
    @Consumes(MediaType.TEXT_PLAIN)
    public void getRegID(@PathParam("varX") String regId){
     PrintWriter out = null;
        try {
            out = new PrintWriter(new BufferedWriter(new FileWriter(REG_ID_STORE, true)));
            out.println(regId);
            System.out.println(regId);
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
    }
    }
    
    
    @GET
    @Path("/rateOffer/{offerId}/{rate}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Override
    public void rateOffer(@PathParam("offerId") String offerId, @PathParam("rate") String rate){
        Offer offer = new Offer();
        offer.setId(Integer.parseInt(offerId));
        offer.setRate(Integer.parseInt(rate));
        offer = offerImpl.calcRate(offer);
    }
    
    
    @GET
    @Path("/rateItem/{itemId}/{rate}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Override
    public void rateItem(@PathParam("itemId") String itemId, @PathParam("rate") String rate){
        OfferItem offerItem = new OfferItem();
        offerItem.setId(Integer.parseInt(itemId));
        offerItem.setRate(Integer.parseInt(rate));
        OfferItemDAOImpl offerItemImpl = new OfferItemDAOImpl();
        if (offerItemImpl.calcRateItem(offerItem)) 
            System.out.println("\nitem rated success\n");
    }
}
