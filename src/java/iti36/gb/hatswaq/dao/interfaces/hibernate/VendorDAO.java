package iti36.gb.hatswaq.dao.interfaces.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Shika
 */
public interface VendorDAO {

    public Vendor checkVendor(String userName, String password);

    public List getAllVendors();

    public List getAllVendorsNames();

    public Vendor getVendorByName(String name);

    public Vendor getVendorByID(Integer id);
}
