/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.interfaces.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Admin;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;

/**
 *
 * @author Aya
 */
public interface AdminDAO {
        
    public Admin checkAdmin(String userName, String password);

    public boolean checkVendorAvailability(Vendor vendor);
    
    public boolean checkVendorUserName(Vendor vendor);
    
    public boolean checkVendorName(Vendor vendor);
    
    public boolean checkVendorLogo(Vendor vendor);
    
    public Vendor getVendor(String userName);
    
    public Admin getAdmin(int adminId);
}
