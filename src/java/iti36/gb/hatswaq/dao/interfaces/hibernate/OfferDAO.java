/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.interfaces.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Shika
 */
public interface OfferDAO {

    public ArrayList<Offer> getOffer(int from, int to);

    public List<Offer> getAllOffers(int vendorId);

    public Offer getOffer(int offerId);

    public Offer updateOffer(Offer offer);

    public List getOfferByVendorSearch(Vendor vendor, Date start, Date end);

    public List getOfferByVendorSearchStart(Vendor vendor, Date start);

    public List getOfferByVendorSearchEnd(Vendor vendor, Date end);

    public List getOfferByVendorVname(Vendor vendor);

    public List getTop10Rated();

    public Offer calcRate(Offer offer);

    public List getAllAcceptOffers();
}
