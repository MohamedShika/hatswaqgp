/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.interfaces.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import java.util.List;

/**
 *
 * @author Shika
 */
public interface AdminCommunicateVendorDAO {

    public List<VendorCommunication> vendorMessages(int id);

    public List<VendorCommunication> adminMessages(int id);

    List<VendorCommunication> adminNotificationsFromVendor(int adminId);
    
    List<VendorCommunication> vendorNotifications(int vendorId);
    
    List<VendorCommunication> vendorNotificationsAll(int vendorId);
}
