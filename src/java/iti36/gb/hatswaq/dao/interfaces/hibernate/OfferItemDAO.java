/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.interfaces.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.OfferItem;
import java.util.List;

/**
 *
 * @author Shika
 */
public interface OfferItemDAO {

    public List<OfferItem> getAllofferItems(Offer offer);

    public boolean calcRateItem(OfferItem offerItem);
}
