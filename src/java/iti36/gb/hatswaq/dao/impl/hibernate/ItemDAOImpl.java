/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Item;
import iti36.gb.hatswaq.dao.interfaces.hibernate.ItemDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;

/**
 *
 * @author Shika
 */
public class ItemDAOImpl extends GenericDAOImpl<Item> implements ItemDAO{
    
}
