/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.interfaces.hibernate.VendorDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Aya
 */
public class VendorDaoImpl extends GenericDAOImpl<Vendor> implements VendorDAO {

    @Override
    public Vendor checkVendor(String userName, String password) {
        Vendor vendor = new Vendor();
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Vendor.class)
                .add(Restrictions.and(Restrictions.eq("username", userName), Restrictions.eq("password", password)));
        List result = criteria.list();
        if (result.isEmpty()) {
            return null;
        } else {
            vendor.setUsername(userName);
            vendor.setPassword(password);
            return (Vendor) result.get(0);
        }
    }

    @Override
    public List getAllVendors() {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Vendor.class)
                .add(Restrictions.eq("enabled", 1));
        List result = criteria.list();
        return result;
    }

    @Override
    public List getAllVendorsNames() {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Vendor.class)
                .setProjection(Projections.property("name")).add(Restrictions.eq("enabled", 1));

        List result = criteria.list();
        return result;
    }
   
    @Override
    public Vendor getVendorByName(String name) {
        Vendor vendor = new Vendor();
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Vendor.class)
                .add(Restrictions.eq("name", name));
        List result = criteria.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Vendor) result.get(0);
        }
    }

    @Override
    public Vendor getVendorByID(Integer id) {
        Vendor vendor = new Vendor();
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Vendor.class)
                .add(Restrictions.eq("id", id));
        List result = criteria.list();
        if (result.isEmpty()) {
            return null;
        } else {
            return (Vendor) result.get(0);
        }
    }
}
