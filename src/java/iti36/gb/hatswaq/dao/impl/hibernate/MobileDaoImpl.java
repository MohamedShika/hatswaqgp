/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Branch;
import iti36.gb.hatswaq.dao.entity.hibernate.Mobile;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.interfaces.hibernate.AdminDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Aya
 */
public class MobileDaoImpl extends GenericDAOImpl<Mobile> {
    Mobile admin = new Mobile();
    public Mobile getMobileGuest(Vendor vendor,Branch branch){
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Mobile.class)
                .add(Restrictions.and(Restrictions.eq("status", false), Restrictions.
                        and(Restrictions.eq("vendor", vendor), Restrictions.eq("branch", branch))));
        //     .add(Restrictions.eq("id.productId", 2))
        List result = criteria.list(); 
        return (Mobile) result.get(0);
    }
}
