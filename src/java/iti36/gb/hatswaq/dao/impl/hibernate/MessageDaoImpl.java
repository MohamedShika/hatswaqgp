/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Message;
import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import iti36.gb.hatswaq.dao.interfaces.hibernate.MessageDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Shika
 */
public class MessageDaoImpl extends GenericDAOImpl<Message> implements MessageDAO {

    @Override
    public List<Object[]> unseenAdminMessages(int id) {
//        List<Object[]> resultList = HibernateUtil.getSessionFactory().openSession().createCriteria(VendorCommunication.class, "vendorCommunication")                
//                .add(Restrictions.eq("vendorCommunication.admin.id", id))
//                .createCriteria("vendorCommunication.message.id", "message")
//                .add(
//                        Restrictions.and(Restrictions.eq("message.seen", false),
//                                Restrictions.and(Restrictions.eq("vendorCommunication.communitcateType", false),
//                                        Restrictions.ne("vendorCommunication.direction", 0))
//                        )
//                )
//                .setProjection(Projections.projectionList()
//                        .add(Projections.property("message.subject"))
//                        .add(Projections.property("message.body"))
//                        .add(Projections.property("message.dateTime"))
//                ).list();
//        return resultList;

//        List<Object[]> resultList = HibernateUtil.getSessionFactory().openSession().createSQLQuery("SELECT m.subject, m.body, m.dateTime FROM Message as m, VendorCommunication as vendorCommunication where m.id = vendorCommunication.message.id and vendorCommunication.admin.id = :givenAdminId  and vendorCommunication.communitcateType = 0 and vendorCommunication.direction <> 0 and m.seen = 0")
//                .setEntity("givenAdminId", new Integer(id)).list();
        List<Object[]> resultList = HibernateUtil.getSessionFactory().openSession().createSQLQuery("SELECT \n" +
"	msg.id, \n" +
"	msg.subject, \n" +
"	msg.body, \n" +
"	msg.date, \n" +
"	vendorCommunication.direction \n" +
"FROM \n" +
"	message as msg, \n" +
"	vendor_communication as vendorCommunication \n" +
"where \n" +
"	msg.id = vendorCommunication.message_id \n" +
"	and vendorCommunication.admin_id = "+ id +" \n" +
"	and vendorCommunication.communitcate_type = 0 \n" +
"	and vendorCommunication.direction <> 0 \n" +
"	and vendorCommunication.seen = 0").list();
        return resultList;
    }
    
    
    public Message getMessage(int msgId){
        return ((Message) HibernateUtil.getSessionFactory().openSession().createCriteria(Message.class).
                add(Restrictions.eq("id", msgId)).uniqueResult());
    }

    public List<Object[]> unseenVendorMessages(int vendorId) {
        List<Object[]> resultList = HibernateUtil.getSessionFactory().openSession().createSQLQuery("SELECT msg.id, msg.subject, msg.body, msg.date, vendorCommunication.direction from message as msg JOIN vendor_communication as vendorCommunication ON msg.id = vendorCommunication.message_id WHERE vendorCommunication.communitcate_type = 0 AND vendorCommunication.direction = 0 AND vendorCommunication.seen = 0 AND vendorCommunication.admin_id = 1 AND vendorCommunication.vendor_id = "+vendorId).list();
        return resultList;
    }
}
