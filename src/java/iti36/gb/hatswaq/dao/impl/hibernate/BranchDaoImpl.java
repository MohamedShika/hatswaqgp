/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Branch;
import iti36.gb.hatswaq.dao.interfaces.hibernate.BranchDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Aya
 */
public class BranchDaoImpl extends GenericDAOImpl<Branch> implements BranchDAO {

    @Override
    public List<Branch> getBranches(int vendorId) {
        return (HibernateUtil.getSessionFactory().openSession().createCriteria(Branch.class).
                add(Restrictions.eq("vendor.id", vendorId)).list());
    }
    public Branch getOneBranche(Branch branch) {
        return (Branch) (HibernateUtil.getSessionFactory().openSession().createCriteria(Branch.class).
                add(Restrictions.eq("id", branch.getId())).uniqueResult());
    }
}
