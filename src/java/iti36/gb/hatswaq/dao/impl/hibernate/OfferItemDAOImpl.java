/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.OfferItem;
import iti36.gb.hatswaq.dao.interfaces.hibernate.OfferItemDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Aya
 */
public class OfferItemDAOImpl extends GenericDAOImpl<OfferItem> implements OfferItemDAO {

    @Override
    public List<OfferItem> getAllofferItems(Offer offer) {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(OfferItem.class).
                add(Restrictions.eq("offer", offer));
        List<OfferItem> items = criteria.list();
        return items;
    }
    
    @Override
    public boolean calcRateItem(OfferItem offerItem){
        OfferItem item = (OfferItem) HibernateUtil.getSessionFactory().openSession().createCriteria(OfferItem.class).
                 add(Restrictions.eq("id", offerItem.getId())).uniqueResult();
        int dbTotalRate = item.getTotalRating();
        int noUsers = item.getUser();
        int newRate = offerItem.getRate();
         
        int calcRate = (int) Math.ceil(((dbTotalRate) + newRate) / (noUsers + 1));
        noUsers++;
        item.setTotalRating(dbTotalRate + newRate);
        item.setRate(calcRate);
        item.setUser(noUsers);
        item = this.update(item);
        return (item.getRate() != newRate);
    }
}
