/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.Resource;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.interfaces.hibernate.ResourceDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Aya
 */
public class ResourceDAOImpl extends GenericDAOImpl<Resource> implements ResourceDAO {
        public List<Resource> getResourceOffers(Offer offer)
        {
         Criteria criteria=HibernateUtil.getSessionFactory().openSession().createCriteria(Resource.class)
                .add(Restrictions.eq("offer",offer));
        List<Resource> result=criteria.list();
        
        return  result ;
        }
}
