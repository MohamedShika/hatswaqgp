/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.VendorCommunication;
import iti36.gb.hatswaq.dao.interfaces.hibernate.AdminCommunicateVendorDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Shika
 */
public class VendorContacatAdminDaoImpl extends GenericDAOImpl<VendorCommunication> implements AdminCommunicateVendorDAO{

    @Override
    public List<VendorCommunication> vendorMessages(int id) {
        return  HibernateUtil.getSessionFactory().openSession().createCriteria(VendorCommunication.class).
                add(Restrictions.and(Restrictions.eq("vendor.id", id), Restrictions.eq("direction", 0)))
                .addOrder(Order.desc("message.id")).list();       
    }

    @Override
    public List<VendorCommunication> adminMessages(int id) {
        return  HibernateUtil.getSessionFactory().openSession().createCriteria(VendorCommunication.class).
                add(Restrictions.and(Restrictions.eq("admin.id", id), Restrictions.ne("direction", 0)))
                .addOrder(Order.desc("message.id")).list();       
    }
    
    @Override
    public List<VendorCommunication> adminNotificationsFromVendor(int adminId) {
        List<VendorCommunication> communications = HibernateUtil.getSessionFactory().openSession().createCriteria(VendorCommunication.class).
                add(
                    Restrictions.and(Restrictions.eq("admin.id", adminId), 
                        Restrictions.and(Restrictions.eq("communitcateType", true), 
                                Restrictions.and(Restrictions.eq("seen", false), Restrictions.eq("direction", 1))
                        )
                    )
                )
                .list();   
        System.out.println("==================================================");
        for (VendorCommunication communication : communications) {
            System.out.println("comm 1 "+communication.getDirection()+ communication.isSeen()+ communication.getMessage().getSubject());
        }
        System.out.println("==================================================");
        return communications;
    }  
    
    
    @Override
    public List<VendorCommunication> vendorNotifications(int vendorId) {
        return  HibernateUtil.getSessionFactory().openSession().createCriteria(VendorCommunication.class).
                add(
                        Restrictions.and(Restrictions.eq("vendor.id", vendorId), 
                                Restrictions.and(Restrictions.eq("direction", 0), 
                                    Restrictions.and(Restrictions.eq("communitcateType", true), 
                                        Restrictions.eq("seen", false)
                                )
                            )
                        )
                )
                .list();       
    }

    
    @Override
    public List<VendorCommunication> vendorNotificationsAll(int vendorId) {
        return  HibernateUtil.getSessionFactory().openSession().createCriteria(VendorCommunication.class).
                add(
                        Restrictions.and(Restrictions.eq("vendor.id", vendorId), 
                                Restrictions.and(Restrictions.eq("direction", 0), 
                                    Restrictions.eq("communitcateType", true)
                                )
                            )
                        )
                .addOrder(Order.desc("message.id")).list();    
    }

    public List<VendorCommunication> adminNotificationsAll(Integer adminId) {
        return  HibernateUtil.getSessionFactory().openSession().createCriteria(VendorCommunication.class).
                add(
                        Restrictions.and(Restrictions.eq("admin.id", adminId), 
                            Restrictions.and(Restrictions.eq("direction", 1), 
                                Restrictions.eq("communitcateType", true)
                            )
                        )
                    )
                .addOrder(Order.desc("message.id")).list();   
    }
}
