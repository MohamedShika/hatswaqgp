/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Admin;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.interfaces.hibernate.AdminDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Aya
 */
public class AdminDaoImpl extends GenericDAOImpl<Admin> implements AdminDAO {

    @Override
    public Admin checkAdmin(String userName, String password) {
        Admin admin = new Admin();
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Admin.class)
                .add(Restrictions.and(Restrictions.eq("username", userName), Restrictions.eq("password", password)));
        //     .add(Restrictions.eq("id.productId", 2))
        List result = criteria.list();
        if (result.isEmpty()) {
            return null;
        } else {

            admin.setUsername(userName);
            admin.setPassword(password);
            System.out.println(result.get(0));
            return (Admin) result.get(0);
        }
    }

    @Override
    public boolean checkVendorAvailability(Vendor vendor) {
        boolean result = false;
        boolean checkUserName = checkVendorName(vendor);
        boolean checkName = checkVendorName(vendor);
        boolean checkLogo = checkVendorLogo(vendor);
        if (checkName && checkUserName && checkLogo) {
            result = true;
        }
        return result;
    }

    @Override
    public boolean checkVendorUserName(Vendor vendor) {
        boolean output = false;
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Vendor.class)
                .add(Restrictions.eq("username", vendor.getUsername()));
        List result = criteria.list();
        if (result.isEmpty()) {
            output = true;
        }
        return output;
    }

    @Override
    public boolean checkVendorName(Vendor vendor) {
        boolean output = false;
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Vendor.class)
                .add(Restrictions.eq("name", vendor.getName()));
        List result = criteria.list();
        if (result.isEmpty()) {
            output = true;
        }

        return output;
    }

    @Override
    public boolean checkVendorLogo(Vendor vendor) {
        boolean output = false;
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Vendor.class)
                .add(Restrictions.eq("logo", vendor.getLogo()));
        List result = criteria.list();
        if (result.isEmpty()) {
            output = true;
        }
        return output;
    }

    @Override
    public Vendor getVendor(String userName) {
        Vendor vendor = new Vendor();
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Vendor.class)
                .add(Restrictions.eq("username", userName));
        List result = criteria.list();
        return (Vendor) result.get(0);
    }

    @Override
    public Admin getAdmin(int adminId) {
        return ((Admin) HibernateUtil.getSessionFactory().openSession().createCriteria(Admin.class).
                add(Restrictions.eq("id", adminId)).uniqueResult());
    }
}
