/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.dao.impl.hibernate;

import iti36.gb.hatswaq.dao.entity.hibernate.Item;
import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import iti36.gb.hatswaq.dao.interfaces.hibernate.OfferDAO;
import iti36.gb.hatswaq.genaric.dao.impl.GenericDAOImpl;
import iti36.gb.hatswaq.sessionfactory.hibernate.HibernateUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Aya
 */
public class OfferDAOImpl extends GenericDAOImpl<Offer> implements OfferDAO {

    @Override
    public ArrayList<Offer> getOffer(int from, int to) {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class);
        criteria.setFirstResult(from);
        criteria.setMaxResults(to);
        List<Offer> offers = criteria.list();
        return (ArrayList<Offer>) offers;
    }

    @Override
    public List<Offer> getAllOffers(int vendorId) {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class).
                add(Restrictions.eq("vendor.id", vendorId));
        List<Offer> offers = criteria.list();
        return offers;
    }

    @Override
    public Offer getOffer(int offerId) {
        return ((Offer) HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class).
                add(Restrictions.eq("id", offerId)).uniqueResult());
    }

    @Override
    public Offer updateOffer(Offer offer) {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class).
                add(Restrictions.eq("id", offer.getId()));
        Offer offerResult = (Offer) criteria.uniqueResult();
        offerResult.setStatus(offer.getStatus());
        HibernateUtil.getSessionFactory().openSession().saveOrUpdate(offerResult);

        return offerResult;
    }

    @Override
    public List getOfferByVendorSearch(Vendor vendor, Date start, Date end) {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class).
                add(Restrictions.and(Restrictions.and(Restrictions.gt("startDate", start), Restrictions.le("startDate", end)), Restrictions.eq("vendor", vendor)));
        List result = criteria.list();
        return result;
    }

    @Override
    public List getOfferByVendorSearchStart(Vendor vendor, Date start) {
        Date end = new Date();
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class).
                add(Restrictions.and(Restrictions.ge("startDate", start), Restrictions.eq("vendor", vendor)));
        List result = criteria.list();
        return result;
    }

    @Override
    public List getOfferByVendorSearchEnd(Vendor vendor, Date end) {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class).
                add(Restrictions.and(Restrictions.le("startDate", end), Restrictions.eq("vendor", vendor)));
        List result = criteria.list();
        return result;
    }

    @Override
    public List getOfferByVendorVname(Vendor vendor) {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class).
                add(Restrictions.eq("vendor", vendor));
        List result = criteria.list();
        return result;
    }

    @Override
    public List getTop10Rated(){
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class).
                add(Restrictions.ge("rate", 4)).addOrder(Order.desc("rate")).setMaxResults(10);
        List result = criteria.list();
        return result;
    }

    @Override
    public Offer calcRate(Offer offer) {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class).
                add(Restrictions.eq("id", offer.getId()));
        List result = criteria.list();
        Offer itemDB = (Offer) result.get(0);
        //------------
        int dbTotalRate = itemDB.getTotalRating();
        int noUsers = itemDB.getUser();
        int newRate = offer.getRate();
        //------------
        int calcRate = (int) Math.ceil(((dbTotalRate) + newRate) / (noUsers + 1));

        noUsers = noUsers + 1;
        itemDB.setTotalRating(dbTotalRate + newRate);
        itemDB.setRate(calcRate);
        itemDB.setUser(noUsers);
        OfferDAOImpl offerDAOImpl = new OfferDAOImpl();
        offerDAOImpl.update(itemDB);
        return itemDB;
    }

    @Override
    public List getAllAcceptOffers() {
        Criteria criteria = HibernateUtil.getSessionFactory().openSession().createCriteria(Offer.class).
                add(Restrictions.eq("status", true));
        List result = criteria.list();
        return result;
    }
}
