/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iti36.gb.hatswaq.pushnotification;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wafik Aziz
 */
public class PushNotification {
    
    private static final String GOOGLE_SERVER_KEY = "AIzaSyCQjyg574jvho06gIJbWJxNowiH01ivz6k";
	static final String MESSAGE_KEY = "message";
	static final String REG_ID_STORE = "E:\\GCMRegId.txt";
        
    
   
    
    private Set readFromFile() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(REG_ID_STORE));
		String regId = "";
		Set regIdSet = new HashSet();
		while ((regId = br.readLine()) != null) {
			regIdSet.add(regId);
		}
		br.close();
		return regIdSet;
	}
    
    
    
    public  void sendPushNotification(String userMessage){
        try {
            MulticastResult result = null;
            Sender sender = new Sender(GOOGLE_SERVER_KEY);
            Message message = new Message.Builder().timeToLive(30)
                    .delayWhileIdle(true).addData(MESSAGE_KEY, userMessage)
                    .build();
            Set regIdSet = readFromFile();
            System.out.println("regId: " + regIdSet);
            List regIdList = new ArrayList();
            regIdList.addAll(regIdSet);
            result = sender.send(message, regIdList, 1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
