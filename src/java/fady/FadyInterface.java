/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fady;

import iti36.gb.hatswaq.dao.entity.hibernate.Item;
import iti36.gb.hatswaq.dao.entity.hibernate.Message;
import iti36.gb.hatswaq.dao.entity.hibernate.Offer;
import iti36.gb.hatswaq.dao.entity.hibernate.Vendor;
import java.util.ArrayList;

/**
 *
 * @author Shika
 */
public interface FadyInterface {
    
    public ArrayList<Item> getListOfItems();

    public Boolean editListOfItems(Item item);

    public Boolean addListOfItems(Item item);

    public Boolean deleteListOfItems(Item item);

    public Boolean addOffer(Offer offer);

    public Boolean editOffer(Offer offer);

    public Boolean sendBroadcastMessage(Message message);

    public Boolean sendMessageToVendor(Message message);

    public Vendor getVendor(String email, String password);
}
